<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$siteDescription = 'Reservation';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $siteDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <?= $this->Html->meta(
        '/img/logo.png',
        '/img/logo.png',
        ['type' => 'icon']
    ); ?>

    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <?= $this->Html->script([ 'jquery.min' ]); ?>
    <?= $this->Html->script([
        '/third-party-assets/js/fullcalendar-index.global.min',
        '/third-party-assets/js/jquery.dataTables.min',
        '/third-party-assets/js/dataTables.buttons.min',
        '/third-party-assets/js/jszip.min',
        '/third-party-assets/js/pdfmake.min',
        '/third-party-assets/js/vfs_fonts',
        '/third-party-assets/js/buttons.html5.min',
        '/third-party-assets/js/buttons.print.min',
        '/third-party-assets/js/dataTables.responsive.min',
        '/third-party-assets/js/moment.min',
        // '/third-party-assets/js/jquery-ui',
        '/third-party-assets/js/sweetalert.min',
    ]); ?>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>

    <?= $this->Html->css([
        '/nice-admin-assets/assets/vendor/bootstrap/css/bootstrap.min',
        '/nice-admin-assets/assets/vendor/bootstrap-icons/bootstrap-icons',
        '/nice-admin-assets/assets/vendor/boxicons/css/boxicons.min',
        '/nice-admin-assets/assets/vendor/quill/quill.snow',
        '/nice-admin-assets/assets/vendor/quill/quill.bubble',
        '/nice-admin-assets/assets/vendor/remixicon/remixicon',
        '/nice-admin-assets/assets/css/style',
        'toastr.min',
        'app',
        '/third-party-assets/css/ladda.min',
        '/third-party-assets/css/jquery.dataTables.min',
        '/third-party-assets/css/buttons.dataTables.min',
        '/third-party-assets/css/responsive.dataTables.min',
        // '/third-party-assets/css/jquery-ui',
    ]) ?>
    <script>
        let BASE_URL = '<?php echo $this->Url->build('/client/'); ?>'
        let IMAGE_BASE_URL = '<?php echo $this->Url->build('/'); ?>'
    </script>
</head>
<body>
    <?= $this->element('client/header') ?>
    <?= $this->element('client/aside') ?>
    <main id="main" class="main">
        <?= $this->Flash->render() ?>
        <?= $this->element('client/breadcrumb') ?>
        <?= $this->fetch('content') ?>
    </main>
    <?= $this->element('client/footer') ?>

    <?= $this->Html->script([
        '/nice-admin-assets/assets/vendor/apexcharts/apexcharts.min',
        '/nice-admin-assets/assets/vendor/bootstrap/js/bootstrap.bundle.min',
        '/nice-admin-assets/assets/vendor/chart.js/chart.umd',
        '/nice-admin-assets/assets/vendor/echarts/echarts.min',
        '/nice-admin-assets/assets/vendor/quill/quill.min',
        '/nice-admin-assets/assets/vendor/tinymce/tinymce.min',
        '/nice-admin-assets/assets/vendor/php-email-form/validate',
        '/nice-admin-assets/assets/js/main',
        'toastr.min',
        'app',
        '/third-party-assets/js/spin.min',
        '/third-party-assets/js/ladda.min',
    ]) ?>

    <script>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "linear",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "slideUp"
        }
    </script>
</body>
</html>
