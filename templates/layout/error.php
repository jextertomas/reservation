<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$siteDescription = 'Reservation';
?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $siteDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <?= $this->Html->meta(
        '/img/logo.png',
        '/img/logo.png',
        ['type' => 'icon']
    ); ?>

    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link rel="stylesheet" href="https://cdn.tutorialjinni.com/Ladda/1.0.0/ladda.min.css" />
    <script src="https://cdn.tutorialjinni.com/Ladda/1.0.0/spin.min.js"></script>
    <script src="https://cdn.tutorialjinni.com/Ladda/1.0.0/ladda.min.js"></script>

    <?= $this->Html->css([
        '/nice-admin-assets/assets/vendor/bootstrap/css/bootstrap.min',
        '/nice-admin-assets/assets/vendor/bootstrap-icons/bootstrap-icons',
        '/nice-admin-assets/assets/vendor/boxicons/css/boxicons.min',
        '/nice-admin-assets/assets/vendor/quill/quill.snow',
        '/nice-admin-assets/assets/vendor/quill/quill.bubble',
        '/nice-admin-assets/assets/vendor/remixicon/remixicon',
        '/nice-admin-assets/assets/vendor/simple-datatables/style',
        '/nice-admin-assets/assets/css/style',
        'toastr.min',
        'app',
    ]) ?>
    <script>
        let BASE_URL = '<?php echo $this->Url->build('/'); ?>'
    </script>
</head>

<body>
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>


    <?= $this->Html->script([
        '/nice-admin-assets/assets/vendor/apexcharts/apexcharts.min',
        '/nice-admin-assets/assets/vendor/bootstrap/js/bootstrap.bundle.min',
        '/nice-admin-assets/assets/vendor/chart.js/chart.umd',
        '/nice-admin-assets/assets/vendor/echarts/echarts.min',
        '/nice-admin-assets/assets/vendor/quill/quill.min',
        '/nice-admin-assets/assets/vendor/simple-datatables/simple-datatables',
        '/nice-admin-assets/assets/vendor/tinymce/tinymce.min',
        '/nice-admin-assets/assets/vendor/php-email-form/validate',
        '/nice-admin-assets/assets/js/main',
        'toastr.min',
        'app',
    ]) ?>
    <script>
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "linear",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "slideUp"
        }
    </script>
</body>

</html>
