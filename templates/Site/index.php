<style>
    .fc-button.fc-button-primary{
        background: maroon !important;
    }
</style>
<section class="calendar-event-container p-0">
    <div class="container">
        <h2 class="mb-3 d-flex justify-content-center">Balay Na Santiago Events</h2>
        <div id='calendar'></div>
    </div>
</section>

<!-- Icons Grid-->
<section class="features-icons bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-window m-auto text-primary"></i></div>
                    <h3>Free Venue Props</h3>
                    <p class="lead mb-0">Venue has props ready for you!</p>
                    <button class="btn btn-sm btn-primary btn-lg btn-block col-12 rounded-20px" id="submitButton" type="submit">Inquire!</button>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-layers m-auto text-primary"></i></div>
                    <h3>Discounted Prices</h3>
                    <p class="lead mb-0">Avail now for discounted prices!</p>
                    <button class="btn btn-sm btn-primary btn-lg btn-block col-12 rounded-20px" id="submitButton" type="submit">Inquire!</button>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex"><i class="bi-terminal m-auto text-primary"></i></div>
                    <h3>Venue Ambiance</h3>
                    <p class="lead mb-0">Beautiful and Nice place!</p>
                    <button class="btn btn-sm btn-primary btn-lg btn-block col-12 rounded-20px" id="submitButton" type="submit">Inquire!</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Image Showcases-->
<section class="showcase">
    <div class="container-fluid p-0">
        <div class="row g-0">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('<?php echo $this->Url->build('/landing-page-assets/assets/img/bg-showcase-1.jpg'); ?>')"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Free Venue Props</h2>
                <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisis leo vel fringilla est ullamcorper. Eget egestas purus viverra accumsan in nisl nisi. Venenatis cras sed felis eget velit aliquet sagittis id consectetur. Dolor sed viverra ipsum nunc aliquet.!</p>
            </div>
        </div>
        <div class="row g-0">
            <div class="col-lg-6 text-white showcase-img" style="background-image: url('<?php echo $this->Url->build('/landing-page-assets/assets/img/bg-showcase-2.jpg'); ?>')"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <h2>Discounted Prices</h2>
                <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisis leo vel fringilla est ullamcorper. Eget egestas purus viverra accumsan in nisl nisi. Venenatis cras sed felis eget velit aliquet sagittis id consectetur. Dolor sed viverra ipsum nunc aliquet.!</p>
            </div>
        </div>
        <div class="row g-0">
            <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('<?php echo $this->Url->build('/landing-page-assets/assets/img/bg-showcase-3.jpg'); ?>')"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Venue Ambiance</h2>
                <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisis leo vel fringilla est ullamcorper. Eget egestas purus viverra accumsan in nisl nisi. Venenatis cras sed felis eget velit aliquet sagittis id consectetur. Dolor sed viverra ipsum nunc aliquet.!</p>
            </div>
        </div>
    </div>
</section>
<!-- Testimonials-->
<section class="testimonials text-center bg-light">
    <div class="container">
        <h2 class="mb-5">Coordinators</h2>
        <div class="row">
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="<?php echo $this->Url->build('/img/placeholder.jpeg'); ?>" alt="..." />
                    <h5>Coordinator 1</h5>
                    <p class="font-weight-light mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.!"</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="<?php echo $this->Url->build('/img/placeholder.jpeg'); ?>" alt="..." />
                    <h5>Coordinator 2</h5>
                    <p class="font-weight-light mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="<?php echo $this->Url->build('/img/placeholder.jpeg'); ?>" alt="..." />
                    <h5>Coordinator 3</h5>
                    <p class="font-weight-light mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.!"</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Call to Action-->
<section class="call-to-action text-white text-center" id="signup">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <h2 class="mb-4">Don't have an account? Sign up now!</h2>
                <form class="form-subscribe" id="contactFormFooter">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-center"><button class="btn btn-sm btn-primary btn-lg btn-block col-8 rounded-20px" id="submitButton" type="submit">Sign Up!</button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        let calendarEl = document.getElementById('calendar');
        let calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            themeSystem: 'bootstrap',
            headerToolbar: {
                left: 'customToday,customDayGridMonth,customListWeek',
                center: 'title',
                right: 'customPrev,customNext'
            },
            footerToolbar: {
                left: 'customPrev,customNext',
                center: 'title',
                right: 'customToday,customDayGridMonth,customListWeek'
            },
            customButtons: {
                customPrev: {
                    text: '◄', // Unicode character for left arrow
                    click: function() {
                        calendar.prev();
                    }
                },
                customNext: {
                    text: '►', // Unicode character for right arrow
                    click: function() {
                        calendar.next();
                    }
                },
                customToday: {
                    text: 'Today',
                    click: function() {
                        calendar.today();
                    }
                },
                customDayGridMonth: {
                    text: 'Month',
                    click: function() {
                        calendar.changeView('dayGridMonth');
                    }
                },
                customListWeek: {
                    text: 'List',
                    click: function() {
                        calendar.changeView('listWeek');
                    }
                }
            },
            eventSources:[
                {
                    url: `${BASE_URL}site/get-full-calendar-events`,
                }
            ],
            eventMouseEnter: function(info) {
                let tooltip = bootstrap.Tooltip.getInstance(info.el);
                if (!tooltip) {
                    tooltip = new bootstrap.Tooltip(info.el, {
                        title: info.event.title,
                        placement: 'top',
                        trigger: 'hover',
                        container: 'body'
                    });
                }
                tooltip.show();
            },
            eventMouseLeave: function(info) {
                let tooltip = bootstrap.Tooltip.getInstance(info.el);
                if (tooltip) {
                    tooltip.hide();
                }
            }
        });
        calendar.render();
    });
</script>
