<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xxl-12 col-md-12">
                    <div class="card info-card">

                        <div class="card-body">
                            <h5 class="card-title">
                                My Account <span>| Information</span>
                            </h5>

                            <div class="row">
                                <!-- Default Tabs -->
                                <ul class="nav nav-tabs d-flex" id="myTabjustified" role="tablist">
                                    <li class="nav-item flex-fill" role="presentation">
                                        <button class="nav-link w-100 active" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-justified" type="button" role="tab" aria-controls="profile" aria-selected="false" tabindex="-1">Account</button>
                                    </li>
                                    <li class="nav-item flex-fill" role="presentation">
                                        <button class="nav-link w-100" id="password-tab" data-bs-toggle="tab" data-bs-target="#password-justified" type="button" role="tab" aria-controls="password" aria-selected="false" tabindex="-1">Password</button>
                                    </li>
                                </ul>
                                <div class="tab-content pt-2" id="myTabjustifiedContent">
                                    <div class="tab-pane fade active show" id="profile-justified" role="tabpanel" aria-labelledby="profile-tab">
                                        <?= $this->Form->create(NULL, ["id" => "profile-form"]) ?>
                                            <div class="row">
                                                <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" name="last_name" class="form-control form-contol-xl shadow-sm" idlast_name" placeholder="Last Name">
                                                        <label for="last_name">Last Name</label>
                                                    </div>
                                                </div>

                                                <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" name="first_name" class="form-control form-contol-xl shadow-sm" placeholder="First Name">
                                                        <label for="first_name">First Name</label>
                                                    </div>
                                                </div>

                                                <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" name="middle_name" class="form-control form-contol-xl shadow-sm" placeholder="Middle Name">
                                                        <label for="middle_name">Middle Name</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <textarea class="form-control form-contol-xl shadow-sm" name="address" placeholder="Address" cols="30" rows="1" placeholder="Address"></textarea>
                                                        <label for="address">Address</label>
                                                    </div>
                                                </div>
                                                <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="text" name="contact_no" class="form-control form-contol-xl shadow-sm" placeholder="Contact Number">
                                                        <label for="contact_no">Contact Number</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="email" name="email" class="form-control form-contol-xl shadow-sm" placeholder="Email">
                                                        <label for="email">Email</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                                            </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                    <div class="tab-pane fade" id="password-justified" role="tabpanel" aria-labelledby="password-tab">
                                        <?= $this->Form->create(NULL, ["id" => "password-form"]) ?>
                                            <div class="row">

                                                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="current_password" name="current_password" class="form-control form-contol-xl shadow-sm" placeholder="Current Password">
                                                        <label for="current_password">Current Password</label>
                                                    </div>
                                                </div>

                                                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="password" name="password" class="form-control form-contol-xl shadow-sm" placeholder="Password">
                                                        <label for="password">Password</label>
                                                    </div>
                                                </div>

                                                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="form-floating mb-3">
                                                        <input type="password" name="confirm_password" class="form-control form-contol-xl shadow-sm" placeholder="Confirm Password">
                                                        <label for="confirm_password">Confirm Password</label>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                                            </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function(){
        let PROFILE_FORM_ELEMENT = $("#profile-form"),
            PASSWORD_FORM_ELEMENT = $("#password-form"),
            PROFILE_TAB_ELEMENT = $("#profile-tab"),
            USER_ID = '<?= $userID ?>'

        PROFILE_TAB_ELEMENT.on("click", (e) => {
            GET_ACCOUNT()
        })

        let PROFILE_FIRST_NAME_INPUT = $('#profile-form input[name="first_name"]'),
            PROFILE_LAST_NAME_INPUT = $('#profile-form input[name="last_name"]'),
            PROFILE_MIDDLE_NAME_INPUT = $('#profile-form input[name="middle_name"]'),
            PROFILE_EMAIL_INPUT = $('#profile-form input[name="email"]'),
            PROFILE_CONTACT_NO_INPUT = $('#profile-form input[name="contact_no"]'),
            PROFILE_ADDRESS_INPUT = $('#profile-form textarea[name="address"]')

        PROFILE_FORM_ELEMENT.add(PASSWORD_FORM_ELEMENT).on("submit", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let l = Ladda.create($(e.target).find('.ladda-button')[0]);
            l.start();
            let FD = new FormData(e.target);
            MAKE_REQUEST(`${BASE_URL}accounts/update/${USER_ID}`, "POST", FD)
            .then(data => {
                l.stop();
                toastr.success(data.message)
                DATATABLE.ajax.reload()
                REMOVE_ERROR_CLASS()
            }).catch(err => {
                l.stop();
                if(err.responseJSON.key){
                    ADD_ERROR_CLASS(err.responseJSON.key)
                }
                toastr.error(err.responseJSON.message)
            })
        })

        const GET_ACCOUNT = () => {
            MAKE_REQUEST(`${BASE_URL}accounts/update/${USER_ID}`)
            .then(data => {
                PROFILE_FIRST_NAME_INPUT.val(data.first_name)
                PROFILE_LAST_NAME_INPUT.val(data.last_name)
                PROFILE_MIDDLE_NAME_INPUT.val(data.middle_name)
                PROFILE_EMAIL_INPUT.val(data.email)
                PROFILE_CONTACT_NO_INPUT.val(data.contact_no)
                PROFILE_ADDRESS_INPUT.val(data.address)
            }).catch(err => {
                toastr.error(err.responseJSON.message)
            })
        }
        GET_ACCOUNT()
    })
</script>