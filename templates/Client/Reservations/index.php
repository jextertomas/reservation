<!-- Modal/s -->
<?= $this->Form->create(NULL, ["id" => "form"]) ?>
    <div class="modal fade" id="modal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reservation Form.</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control form-contol-xl shadow-sm" name="title" id="title" placeholder="Event Title">
                                <label for="title" style="color: gray;">Event Title</label>
                            </div>
                        </div>

                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-floating mb-3">
                                <textarea class="form-control form-contol-xl shadow-sm" name="description" placeholder="Event's Description" id="description" style="height: 100px;"></textarea>
                                <label for="description" style="color: gray;">Event's Description</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="form-floating mb-3">
                                    <select name="classification" class="form-control form-contol-xl shadow-sm" placeholder="Classification" id="classification">
                                        <option value="">-</option>
                                        <option value="public">Public</option>
                                        <option value="private">Private</option>
                                    </select>
                                    <label for="classification" style="color: gray;">Classification</label>
                                </div>
                            </div>
                            
                            <div class="col col-lg-5 col-md-5 col-sm-11 col-11" id="document-input">
                                <div class="form-floating mb-3">
                                    <input type="file" class="form-control form-contol-xl shadow-sm" name="attachment-file" id="attachment-file" accept="application/pdf,.docx">
                                    <label for="attachment-file" style="color: gray; background: none;">Document</label>
                                </div>
                            </div>
                            <div class="col col-lg-1 col-md-1 col-sm-1 col-1 mt-3 p-0" id="download-button">
                                <a href="#" class="btn btn-primary btn-sm rounded-1 rounded-20px" id="download-attachment" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Download previously uploaded document.">
                                    <i class="bi bi-download text-white"></i>
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control form-contol-xl shadow-sm datetimepicker" name="date" id="date" placeholder="Event Date">
                                    <label for="date" style="color: gray;">Event Date</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group form-control-file-container col-md-12 col-lg-12 col-sm-12 col-12">
                                <label for="image-field" class="form-control-file-label">
                                    <h3 style="color: gray;">Click here to select or drag and drop image receipt here..</h3>
                                </label>
                                <input type="file" class="form-control-file" id="image-field" name="image-field" accept="'image/*'">
                                <span class="form-control-clear-button" style="color: gray;">✖</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary rounded-1 rounded-20px" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                </div>
            </div>
        </div>
    </div><!-- End Full Screen Modal-->
<?= $this->Form->end() ?>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xxl-12 col-md-12">
                    <div class="card info-card">

                        <div class="card-body">
                            <h5 class="card-title">
                                
                                <div class="row">
                                    <div class="col col-lg-6 col-md-6 col-sm-6 col-12 d-flex justify-content-start align-items-center">
                                        <div class="col col-lg-6 col-md-8 col-sm-12 col-12 d-flex justify-content-sm-start justify-content-md-start justify-content-lg-start justify-content-xl-start justify-content-center">
                                            Reservations <span class="d-flex align-items-center">| List</span>
                                        </div>
                                    </div>

                                    <div class="col col-lg-6 col-md-6 col-sm-6 col-12 d-flex justify-content-end">
                                        <button class="btn btn-primary rounded-1 rounded-20px col-lg-6 col-md-8 col-sm-12 col-12" id="create" type="button">
                                            Create Reservation
                                        </button>
                                    </div>
                                </div>
                                
                            </h5>

                            <div class="row">
                                <div class="col col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="form-floating mb-1">
                                        <select class="form-select form-contol-xl shadow-sm" id="order-direction" aria-label="Floating label select example">
                                            <option selected value="ASC">Ascending</option>
                                            <option value="DESC">Descending</option>
                                        </select>
                                        <label for="order-direction">Sort By Direction: </label>
                                    </div>
                                </div>
                                <div class="col col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="form-floating mb-1">
                                        <select class="form-select form-contol-xl shadow-sm" id="order-by" aria-label="Floating label select example">
                                            <option selected value="title">Title</option>
                                            <option value="description">Description</option>
                                            <option value="date">Event Date</option>
                                            <option value="created">Created Date</option>
                                        </select>
                                        <label for="order-by">Sort By: </label>
                                    </div>
                                </div>
                                
                                <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="form-floating mb-1">
                                        <select class="form-select form-contol-xl shadow-sm" id="filter-by" aria-label="Floating label select example">
                                            <option selected value="">All</option>
                                            <option value="pending">Pending</option>
                                            <option value="approved">Approved</option>
                                            <option value="disapproved">Disapproved</option>
                                        </select>
                                        <label for="filter-by">Filter By: </label>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <table class="table display nowrap" id="datatable" style="width:100%; border-top: 1px solid gray;">
                                <thead hidden>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function(){
        let DATATABLE_ELEMENT = $("#datatable"),
            ORDER_DIRECTION_ELEMENT = $("#order-direction"),
            ORDER_BY_ELEMENT = $("#order-by"),
            FILTER_BY_ELEMENT = $("#filter-by"),
            MODAL_ELEMENT = $("#modal"),
            CREATE_ELEMENT = $("#create"),
            FORM_ELEMENT = $("#form"),
            RESERVATION_ID = null,
            DELETE_IMAGE = false

        // ============== Datatables events
        let DATATABLE = DATATABLE_ELEMENT.DataTable({
            dom: 'Blfrtip',
            processing: false,
            lengthMenu: false,
            lengthChange: false,
            paging: true,
            searching: true,
            serverSide: true,
            saveState: true,
            responsive: true,
            pageLength: 10,
            order: [
                [0, 'ASC']
            ],
            bInfo: true,
            buttons: [
                {
                    extend: 'excel',
                    footer: true,
                    text: 'EXCEL',
                    filename: 'Reservations_Excel_'+new Date().toISOString().split('T')[0],
                    exportOptions: {
                        columns: ':not(:first-child)'
                    },
                },
                {
                    extend: 'pdf',
                    footer: true,
                    text: 'PDF',
                    filename: 'Reservations_PDF_'+new Date().toISOString().split('T')[0],
                    exportOptions: {
                        columns: ':not(:first-child)'
                    },
                },
                {
                    extend: 'print',
                    footer: true,
                    text: 'PRINT',
                    filename: 'Reservations_Print_'+new Date().toISOString().split('T')[0],
                    exportOptions: {
                        columns: ':not(:first-child)'
                    },
                }
            ],
            ajax: {
                url: `${BASE_URL}reservations/getAllReservations`,
                type: "GET",
                data: function(data) {
                    data.page = data.start / data.length + 1
                    data.order_direction = ORDER_DIRECTION_ELEMENT.val()
                    data.order_by = ORDER_BY_ELEMENT.val()
                    data.filter_by = FILTER_BY_ELEMENT.val()
                },
                beforeSend: function() {
                    $('#datatable > tbody').html(
                        `<tr><td align="top" class="text-success" colspan="100" style="text-align:center">
                            <div class="d-flex justify-content-center">
                                <div class="spinner-border" role="status" style="color: blue;">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        </td></tr>`
                    );
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.responseJSON.message)
                },
            },
            columns: [
                {
                    data: "title",
                    orderable: true,
                    width: "10%",
                    render: function(data,type,row) {
                        return `<div class="avatar data_id" data-id="${row.id}">${data.charAt(0).toUpperCase()}</div>`
                    }
                },
                {
                    data: "title",
                    orderable: true,
                    width: "20%",
                    render: function(data,type,row) {
                        let statusColor = 'info';
                        if(row.status === 'approved') statusColor = "success"; else if(row.status === 'disapproved') statusColor = "danger"; 
                        let paymentStatusColor = 'info';
                        if(row.payment_status === 'paid') paymentStatusColor = "success"; else if(row.payment_status === 'unpaid') paymentStatusColor = "danger"; 
                        let name = `${row._matchingData.Users.first_name} ${row._matchingData.Users.middle_name} ${row._matchingData.Users.last_name}`
                        return `<p class="title">${data} 
                                <span class="badge rounded-pill bg-${statusColor}" style="text-transform: capitalize;">${row.status}</span>
                                ${(row.payment_status != null) ? `<span class="badge rounded-pill bg-${paymentStatusColor}" style="text-transform: capitalize;">${row.payment_status}</span>`:``}
                            </p>   
                            <p class="description">${row.description}</p>
                            <p class="creator">Created By: ${name}</p>`
                    }
                },
                {
                    data: "date",
                    className: "text-right",
                    orderable: true,
                    render: function(data,type,row) {
                        return `<p class="date-event">Event Date: ${moment(data).format('MMMM DD, YYYY')}</p>
                            <p class="date-created">Created: ${moment(row.created).format('MMMM DD, YYYY')}</p>`
                    }
                },
                {
                    data: "id",
                    className: "text-right",
                    orderable: true,
                    render: function(data,type,row) {
                        if(row.status === "disapproved"){
                            return `<i class="bi bi-arrow-clockwise text-danger cancelled" data-id="${row.id}"></i>`
                        }else{
                            return `<i class="bi bi-trash text-danger delete" data-id="${row.id}"></i>`
                        }
                    }
                },

            ]
        })
        DATATABLE.on('click', 'tbody tr', (e) => {
            let ID = $(e.currentTarget).find(".data_id").data("id")
            let classList = e.currentTarget.classList;

            if (classList.contains('selected')) {
                RESERVATION_ID = null
                classList.remove('selected');
            } else {
                if(ID){
                    RESERVATION_ID = ID
                    GET_RESERVATION()
                }
                REMOVE_SELECTED_ROW()
                classList.add('selected');
            }
        });

        MODAL_ELEMENT.on('hidden.bs.modal', (e) => {
            RESERVATION_ID = null
            FORM_ELEMENT[0].reset()
            CLEAR_IMAGE()
            DELETE_IMAGE = false
            REMOVE_ERROR_CLASS()
            REMOVE_SELECTED_ROW()
            GET_ALL_SELECTED_DATES()
        });

        CREATE_ELEMENT.on("click", (e) => {
            e.preventDefault();
            $("#document-input").removeClass("col-lg-5 col-md-5 col-sm-11 col-11").addClass("col-lg-6 col-md-6 col-sm-12 col-12")
            $("#download-button").hide()
            MODAL_ELEMENT.modal("show")
        })

        ORDER_DIRECTION_ELEMENT.add(ORDER_BY_ELEMENT).add(FILTER_BY_ELEMENT).on("change", () => {
            DATATABLE.ajax.reload()
        })

        const REMOVE_SELECTED_ROW = () => {
            DATATABLE.rows('.selected').nodes().each((row) => row.classList.remove('selected'));
        }

        // ============== Form events
        let TITLE_INPUT = $("#title"),
            DESCRIPTION_INPUT = $("#description"),
            STATUS_INPUT = $("#status"),
            CLASSIFICATION_INPUT = $("#classification"),
            DATE_INPUT = $("#date"),
            ATTACHMENT_FILE_INPUT = $("#attachment-file")

        ATTACHMENT_FILE_INPUT.on("change", (e) => {
            CHECK_FILE_SIZE(e.target)
        })

        DATATABLE.on("click", ".delete", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let ID = $(e.target).data("id")
            SWAL_CONFIRM("Delete", "Are you sure you want to delete this reservation?", "warning", () => {
                MAKE_REQUEST(`${BASE_URL}reservations/delete/${ID}`, "DELETE")
                .then(data => {
                    toastr.success(data.message)
                    DATATABLE.ajax.reload()
                    swal.close();
                }).catch(err => {
                    toastr.error(err.responseJSON.message)
                })
            })
        })
        DATATABLE.on("click", ".cancelled", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let ID = $(e.target).data("id")
            SWAL_OPTIONS("Cancelled", "This reservation has been cancelled, you can re-submit or completely delete this reservation?", "info", 
                () => {
                    swal.close();
                    SWAL_CONFIRM("Re Submit", "Are you sure you want to re-submit this reservation?", "warning", () => {
                        MAKE_REQUEST(`${BASE_URL}reservations/reSubmit/${ID}`, "DELETE")
                        .then(data => {
                            toastr.success(data.message)
                            DATATABLE.ajax.reload()
                            swal.close();
                        }).catch(err => {
                            toastr.error(err.responseJSON.message)
                        })
                    })
                }, 
                () => {
                    swal.close();
                    SWAL_CONFIRM("Delete", "Are you sure you want to delete this reservation?", "warning", () => {
                        MAKE_REQUEST(`${BASE_URL}reservations/delete/${ID}`, "DELETE")
                        .then(data => {
                            toastr.success(data.message)
                            DATATABLE.ajax.reload()
                            swal.close();
                        }).catch(err => {
                            toastr.error(err.responseJSON.message)
                        })
                    })
                }
            )
        })
        FORM_ELEMENT.on("submit", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let l = Ladda.create($(e.target).find('.ladda-button')[0]);
            l.start();
            let FD = new FormData(e.target);
            if(RESERVATION_ID){
                FD.append('will-delete-image', DELETE_IMAGE);
            }
            MAKE_REQUEST(`${BASE_URL}reservations/${(RESERVATION_ID == null) ? "create":`update/${RESERVATION_ID}`}`, "POST", FD)
            .then(data => {
                l.stop();
                toastr.success(data.message)
                DATATABLE.ajax.reload()
                MODAL_ELEMENT.modal("hide")
                REMOVE_ERROR_CLASS()
            }).catch(err => {
                l.stop();
                if(err.responseJSON.key){
                    ADD_ERROR_CLASS(err.responseJSON.key)
                }
                toastr.error(err.responseJSON.message)
            })
        })
        const GET_RESERVATION = () => {
            MAKE_REQUEST(`${BASE_URL}reservations/update/${RESERVATION_ID}`)
            .then(data => {
                TITLE_INPUT.val(data.title)
                DESCRIPTION_INPUT.val(data.description)
                STATUS_INPUT.val(data.status)
                CLASSIFICATION_INPUT.val(data.classification)
                DATE_INPUT.val(moment(data.date).format("YYYY-MM-DD"))
                if(data.official_reciept){
                    SHOW_IMAGE(`${IMAGE_BASE_URL}reciept-images/${data.official_reciept}`)
                }
                $("#download-attachment").attr("data-file-name", data.attachment)
                $("#download-attachment").find("i").attr("data-file-name", data.attachment)
                $("#document-input").removeClass("col-lg-6 col-md-6 col-sm-12 col-12").addClass("col-lg-5 col-md-5 col-sm-11 col-11")
                $("#download-button").show()
                MODAL_ELEMENT.modal("show")
            }).catch(err => {
                toastr.error(err.responseJSON.message)
            })
        }
        $("#download-attachment").on("click", (e) => {
            e.stopPropagation();
            e.preventDefault();
            
            let dataFileName = $(e.target).attr("data-file-name")
            console.log(dataFileName, e.target)
            window.open(`${BASE_URL}reservations/download/${dataFileName}`, '_blank')
        })

        // ============== Image change event
        let IMAGE_FIELD_ELEMENT = $('#image-field'),
            FILE_LABEL_ELEMENT = $(".form-control-file-label"),
            CLEAR_BUTTON_ELEMENT = $(".form-control-clear-button")
        IMAGE_FIELD_ELEMENT.on('change', (evt) => {
            $(".form-control-file-label").removeClass("error-field-file")
            CHECK_FILE_SIZE(evt.target)
            let files = evt.target.files;
            if (files.length > 0) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    SHOW_IMAGE(e.target.result);
                };
                reader.readAsDataURL(files[0]);
                FILE_LABEL_ELEMENT.css({'border': '2px dashed rgb(202, 202, 202)'})
            } else {
                FILE_LABEL_ELEMENT.html('<h3 style="color: gray;">Click here to select or drag and drop image receipt here..</h3>');
                CLEAR_BUTTON_ELEMENT.css({"display":"none"});
            }
        })
        CLEAR_BUTTON_ELEMENT.on('click', () => {
            CLEAR_IMAGE();
        });
        const CLEAR_IMAGE = () => {
            $(".form-control-file-label").removeClass("error-field-file")
            DELETE_IMAGE = true
            IMAGE_FIELD_ELEMENT.val("");
            FILE_LABEL_ELEMENT.html('<h3 style="color: gray;">Click here to select or drag and drop image receipt here..</h3>');
            CLEAR_BUTTON_ELEMENT.css({"display":"none"});
        }
        const SHOW_IMAGE = (image) => {
            DELETE_IMAGE = false
            FILE_LABEL_ELEMENT.html(`<img src="${image}" style="max-width: 100%; max-height: 200px;" loading="lazy" alt="Recipet Image" />`);
            CLEAR_BUTTON_ELEMENT.css({"display":"block"});
        }

        // ============== Datetime picker
        let DATETIME_PICKER_ELEMENT = $(".datetimepicker"),
            DISABLED_DATES
        DATETIME_PICKER_ELEMENT.datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShowDay: function(date) {
                // let disabledDates = ["2024-04-20", "2024-04-25", "2024-04-28", $("#date").val()];
                let formattedDate = $.datepicker.formatDate('yy-mm-dd', date);
                if ($.inArray(formattedDate, DISABLED_DATES) != -1) {
                    return [false];
                }
                return [true];
            },
            startView: 'months',
            minViewMode: 'months',
            showOtherMonths: true,
            selectOtherMonths: true,
        })
        const GET_ALL_SELECTED_DATES = () => {
            MAKE_REQUEST(`${BASE_URL}reservations/getAllSelectedDates`)
            .then(data => {
                DISABLED_DATES = data
            }).catch(err => {
                toastr.error(err.responseJSON.message)
            })
        }
        GET_ALL_SELECTED_DATES()

        // ====================== PUSHER FOR REALTIME
        Pusher.logToConsole = false;
        let pusher = new Pusher('760474ced8703eaea48f', {
            cluster: 'us2'
        });

        let myChannel = pusher.subscribe("<?= $userID ?>-channel");
        myChannel.bind("<?= $userID ?>-event", function(data) {
            DATATABLE.ajax.reload()
            toastr.success(data.message)
        });
    })
</script>
