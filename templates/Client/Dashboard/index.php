<section class="section dashboard">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4 col-xxl-4">
                    <div class="card info-card sales-card">

                        <div class="card-body">
                            <h5 class="card-title">Reservations <span>| Today</span></h5>

                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-calendar-check"></i>
                                </div>
                                <div class="ps-3">
                                    <h6><?= $reservationThisDay ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xxl-4">
                    <div class="card info-card sales-card">

                        <div class="card-body">
                            <h5 class="card-title">Reservations <span>| This Month</span></h5>

                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-calendar-check"></i>
                                </div>
                                <div class="ps-3">
                                    <h6><?= $reservationThisMonth ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xxl-4">
                    <div class="card info-card sales-card">

                        <div class="card-body">
                            <h5 class="card-title">Reservations <span>| This Year</span></h5>

                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-calendar-check"></i>
                                </div>
                                <div class="ps-3">
                                    <h6><?= $reservationThisYear ?></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5 col-xxl-5">
                    <div class="card">

                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Action</h6>
                                </li>

                                <li><a class="dropdown-item" id="refresh-chart" href="#">Refresh Chart</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Reservations <span>| Chart</span></h5>
                            <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-floating mb-3">
                                    <select name="chart_year" class="form-control form-contol-sm shadow-sm" placeholder="Year" id="chart_year">
                                        <?php
                                            $current_year = date("Y");
                                            $past_years_count = 5;
                                            for ($i = $current_year; $i >= $current_year - $past_years_count; $i--) {
                                                echo "<option value='$i' ".(($current_year == $i) ? "selected":"").">$i</option>";
                                            }
                                        ?>
                                    </select>
                                    <label for="chart_year" style="color: gray;">Year</label>
                                </div>
                            </div>
                            <div id="columnChart"></div>

                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-7 col-lg-7 col-xxl-7">
                    <div class="card info-card">

                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Action</h6>
                                </li>

                                <li><a class="dropdown-item" id="refresh-calendar" href="#">Refresh Calendar</a></li>
                            </ul>
                        </div>

                        <div class="card-body">
                            <h5 class="card-title">Reservations <span>| Calendar</span></h5>

                            <div id='calendar'></div>
                        </div>

                    </div>
                </div>



            </div>
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        let calendarEl = document.getElementById('calendar');
        let calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            themeSystem: 'bootstrap',
            headerToolbar: {
                left: 'customToday,customDayGridMonth,customListWeek',
                center: 'title',
                right: 'customPrev,customNext'
            },
            footerToolbar: {
                left: 'customPrev,customNext',
                center: 'title',
                right: 'customToday,customDayGridMonth,customListWeek'
            },
            customButtons: {
                customPrev: {
                    text: '◄', // Unicode character for left arrow
                    click: function() {
                        calendar.prev();
                    }
                },
                customNext: {
                    text: '►', // Unicode character for right arrow
                    click: function() {
                        calendar.next();
                    }
                },
                customToday: {
                    text: 'Today',
                    click: function() {
                        calendar.today();
                    }
                },
                customDayGridMonth: {
                    text: 'Month',
                    click: function() {
                        calendar.changeView('dayGridMonth');
                    }
                },
                customListWeek: {
                    text: 'List',
                    click: function() {
                        calendar.changeView('listWeek');
                    }
                }
            },
            eventSources: [{
                url: `${BASE_URL}dashboard/get-full-calendar-events`,
            }],
            eventMouseEnter: function(info) {
                let tooltip = bootstrap.Tooltip.getInstance(info.el);
                if (!tooltip) {
                    tooltip = new bootstrap.Tooltip(info.el, {
                        title: info.event.title,
                        placement: 'top',
                        trigger: 'hover',
                        container: 'body'
                    });
                }
                tooltip.show();
            },
            eventMouseLeave: function(info) {
                let tooltip = bootstrap.Tooltip.getInstance(info.el);
                if (tooltip) {
                    tooltip.hide();
                }
            }
        });
        calendar.render();
        $("#refresh-calendar").on("click", (e) => {
            e.preventDefault()
            calendar.refetchEvents()
        })

        let apexChart = new ApexCharts(document.querySelector("#columnChart"), {
            series: [{
                name: 'Approved',
                data: Array(12).fill(0)
            }, {
                name: 'Disapproved',
                data: Array(12).fill(0)
            }, {
                name: 'Pending',
                data: Array(12).fill(0)
            }],
            colors: [
                '#008000',
                '#Ff0000',
                'yellow'
            ],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            },
            yaxis: {
                title: {
                    text: 'Reservation Counts'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return "" + val + " total reservations"
                    }
                }
            }
        })
        apexChart.render();
        $("#refresh-chart").on("click", (e) => {
            e.preventDefault()
            getReservations($("#chart_year").val())
        })
        $("#chart_year").on("change", (e) => {
            getReservations($(e.target).val())
        })

        function getReservations(YEAR) {
            MAKE_REQUEST(`${BASE_URL}dashboard/getReservationsByMonth${(YEAR != '') ? `/${YEAR}`:''}`)
                .then(data => {
                    apexChart.updateSeries([
                        { data: data.approved },
                        { data: data.disapproved },
                        { data: data.pending },
                    ], false)
                }).catch(err => {
                    console.log(err)
                    toastr.error(err.responseJSON.message)
                })
        }
        getReservations($("#chart_year").val())

    });
</script>
