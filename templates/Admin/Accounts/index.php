<!-- Modal/s -->
<!-- Create modal -->
<?= $this->Form->create(NULL, ["id" => "form"]) ?>
    <div class="modal fade" id="modal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Account Form.</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="last_name" class="form-control form-contol-xl shadow-sm" idlast_name" placeholder="Last Name">
                            <label for="last_name">Last Name</label>
                        </div>
                    </div>

                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="first_name" class="form-control form-contol-xl shadow-sm" placeholder="First Name">
                            <label for="first_name">First Name</label>
                        </div>
                    </div>

                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="middle_name" class="form-control form-contol-xl shadow-sm" placeholder="Middle Name">
                            <label for="middle_name">Middle Name</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="contact_no" class="form-control form-contol-xl shadow-sm" placeholder="Contact Number">
                            <label for="contact_no">Contact Number</label>
                        </div>
                    </div>
                    <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <textarea class="form-control form-contol-xl shadow-sm" name="address" placeholder="Address" cols="30" rows="1" placeholder="Address"></textarea>
                            <label for="address">Address</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <select name="role" class="form-control form-contol-xl shadow-sm" placeholder="Role">
                                <option value="">-- Select Role --</option>
                                <option value="admin">Admin</option>
                                <option value="client">Client</option>
                            </select>
                            <label for="role" style="color: gray;">Role</label>
                        </div>
                    </div>

                    <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="email" name="email" class="form-control form-contol-xl shadow-sm" placeholder="Email">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="password" name="password" class="form-control form-contol-xl shadow-sm" placeholder="Password">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="password" name="confirm_password" class="form-control form-contol-xl shadow-sm" placeholder="Confirm Password">
                            <label for="confirm_password">Confirm Password</label>
                        </div>
                    </div>

                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary rounded-1 rounded-20px" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                </div>
            </div>
        </div>
    </div><!-- End Full Screen Modal-->
<?= $this->Form->end() ?>

<!-- Profile modal -->
<?= $this->Form->create(NULL, ["id" => "profile-form"]) ?>
    <div class="modal fade" id="profile-modal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Account Form.</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="last_name" class="form-control form-contol-xl shadow-sm" idlast_name" placeholder="Last Name">
                            <label for="last_name">Last Name</label>
                        </div>
                    </div>

                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="first_name" class="form-control form-contol-xl shadow-sm" placeholder="First Name">
                            <label for="first_name">First Name</label>
                        </div>
                    </div>

                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="middle_name" class="form-control form-contol-xl shadow-sm" placeholder="Middle Name">
                            <label for="middle_name">Middle Name</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="text" name="contact_no" class="form-control form-contol-xl shadow-sm" placeholder="Contact Number">
                            <label for="contact_no">Contact Number</label>
                        </div>
                    </div>
                    <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <textarea class="form-control form-contol-xl shadow-sm" name="address" placeholder="Address" cols="30" rows="1" placeholder="Address"></textarea>
                            <label for="address">Address</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <select name="role" class="form-control form-contol-xl shadow-sm" placeholder="Role">
                                <option value="">-- Select Role --</option>
                                <option value="admin">Admin</option>
                                <option value="client">Client</option>
                            </select>
                            <label for="role" style="color: gray;">Role</label>
                        </div>
                    </div>

                    <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="email" name="email" class="form-control form-contol-xl shadow-sm" placeholder="Email">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary rounded-1 rounded-20px" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                </div>
            </div>
        </div>
    </div><!-- End Full Screen Modal-->
<?= $this->Form->end() ?>

<!-- Password modal -->
<?= $this->Form->create(NULL, ["id" => "password-form"]) ?>
    <div class="modal fade" id="password-modal" tabindex="-1">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Account Form.</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="password" name="password" class="form-control form-contol-xl shadow-sm" placeholder="Password">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-floating mb-3">
                            <input type="password" name="confirm_password" class="form-control form-contol-xl shadow-sm" placeholder="Confirm Password">
                            <label for="confirm_password">Confirm Password</label>
                        </div>
                    </div>

                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary rounded-1 rounded-20px" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary rounded-1 rounded-20px ladda-button" data-style="zoom-out" data-color="blue">Save changes</button>
                </div>
            </div>
        </div>
    </div><!-- End Full Screen Modal-->
<?= $this->Form->end() ?>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xxl-12 col-md-12">
                    <div class="card info-card">

                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="row">
                                    <div class="col col-lg-6 col-md-6 col-sm-6 col-12 d-flex justify-content-start align-items-center">
                                        <div class="col col-lg-6 col-md-8 col-sm-12 col-12 d-flex justify-content-sm-start justify-content-md-start justify-content-lg-start justify-content-xl-start justify-content-center">
                                            Users Account <span class="d-flex align-items-center">| List</span>
                                        </div>
                                    </div>

                                    <div class="col col-lg-6 col-md-6 col-sm-6 col-12 d-flex justify-content-end">
                                        <button class="btn btn-primary rounded-1 rounded-20px col-lg-6 col-md-8 col-sm-12 col-12" id="create" type="button">
                                        Create Account
                                        </button>
                                    </div>
                                </div>
                            </h5>

                            <div class="row">
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-floating mb-1">
                                        <select class="form-select" id="order-direction" aria-label="Floating label select example">
                                            <option selected value="ASC">Ascending</option>
                                            <option value="DESC">Descending</option>
                                        </select>
                                        <label for="order-direction">Filter By: </label>
                                    </div>
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-floating mb-1">
                                        <select class="form-select" id="order-by" aria-label="Floating label select example">
                                            <option selected value="first_name">First Name</option>
                                            <option value="last_name">Last Name</option>
                                            <option value="middle_name">Middle Name</option>
                                            <option value="email">Email</option>
                                            <option value="role">Role</option>
                                        </select>
                                        <label for="order-by">Filter By: </label>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <table class="table display nowrap" id="datatable" style="width:100%; border-top: 1px solid gray;">
                                <thead hidden>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(function(){
        let DATATABLE_ELEMENT = $("#datatable"),
            ORDER_DIRECTION_ELEMENT = $("#order-direction"),
            ORDER_BY_ELEMENT = $("#order-by"),
            CREATE_ELEMENT = $("#create"),
            MODAL_ELEMENT = $("#modal"),
            PROFILE_MODAL_ELEMENT = $("#profile-modal"),
            PASSWORD_MODAL_ELEMENT = $("#password-modal"),
            FORM_ELEMENT = $("#form"),
            PROFILE_FORM_ELEMENT = $("#profile-form"),
            PASSWORD_FORM_ELEMENT = $("#password-form"),
            USER_ID = null

        // ============== Datatables events
        let DATATABLE = DATATABLE_ELEMENT.DataTable({
            dom: 'Blfrtip',
            processing: false,
            lengthMenu: false,
            lengthChange: false,
            paging: true,
            searching: true,
            serverSide: true,
            saveState: true,
            responsive: true,
            pageLength: 10,
            order: [
                [0, 'ASC']
            ],
            bInfo: true,
            buttons: [
                'copy'
                // {
                //     extend: 'excel',
                //     footer: true,
                //     text: 'EXCEL',
                //     filename: 'Accounts_Excel_'+new Date().toISOString().split('T')[0],
                //     exportOptions: {
                //         columns: ':not(:first-child)'
                //     },
                // },
                // {
                //     extend: 'pdf',
                //     footer: true,
                //     text: 'PDF',
                //     filename: 'Accounts_PDF_'+new Date().toISOString().split('T')[0],
                //     exportOptions: {
                //         columns: ':not(:first-child)'
                //     },
                // },
                // {
                //     extend: 'print',
                //     footer: true,
                //     text: 'PRINT',
                //     filename: 'Accounts_Print_'+new Date().toISOString().split('T')[0],
                //     exportOptions: {
                //         columns: ':not(:first-child)'
                //     },
                // }
            ],
            ajax: {
                url: `${BASE_URL}accounts/getAllUsers`,
                type: "GET",
                data: function(data) {
                    data.page = data.start / data.length + 1
                    data.order_direction = ORDER_DIRECTION_ELEMENT.val()
                    data.order_by = ORDER_BY_ELEMENT.val()
                },
                beforeSend: function() {
                    $('#datatable > tbody').html(
                        `<tr><td align="top" class="text-success" colspan="100" style="text-align:center">
                            <div class="d-flex justify-content-center">
                                <div class="spinner-border" role="status" style="color: blue;">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        </td></tr>`
                    );
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.responseJSON.message)
                },
            },
            columns: [
                {
                    data: "id",
                    orderable: true,
                    width: "10%",
                    render: function(data,type,row) {
                        return `<div class="avatar data_id" data-id="${row.id}">${row.first_name.charAt(0).toUpperCase()}</div>`
                    }
                },
                {
                    data: "id",
                    orderable: true,
                    width: "20%",
                    render: function(data,type,row) {
                        let name = `${row.last_name}, ${row.first_name}, ${row.middle_name}`
                        return `<p class="title">${name}
                            <span class="badge rounded-pill bg-info" style="text-transform: capitalize;">${row.role}</span>
                            ${(row.is_active == 0) ? `<span class="badge rounded-pill bg-danger" style="text-transform: capitalize;">Locked</span></p>`:""}
                            <p class="description">${row.contact_no} | ${row.address}</p>`
                    }
                },
                {
                    data: "id",
                    className: "text-right",
                    orderable: true,
                    render: function(data,type,row) {
                        return `<p class="date-event">Email: ${row.email}</p>
                            <p class="date-created">Created: ${moment(row.created).format('MMMM DD, YYYY')}</p>`
                    }
                },
                {
                    data: "id",
                    className: "text-right",
                    orderable: true,
                    render: function(data,type,row) {
                        if(row.is_active == 1){
                            return `<i class="bx bxs-lock-alt text-danger update-is-active" data-id="${row.id}" data-value="0"></i>`
                        }else{
                            return `<i class="bx bxs-lock-open-alt text-danger update-is-active" data-id="${row.id}" data-value="1"></i>`
                        }
                    }
                },

            ],
            rowCallback: function(row, data, index) {
                $(row).attr('data-action', 'show-actions-menu');
                $(row).attr('data-fm-floatTo', 'top');
            }
        })
        // DATATABLE.on('click', 'tbody tr', (e) => {
        //     let ID = $(e.currentTarget).find(".data_id").data("id")
        //     let classList = e.currentTarget.classList;

        //     if (classList.contains('selected')) {
        //         USER_ID = null
        //         classList.remove('selected');
        //     } else {
        //         if(ID){
        //             USER_ID = ID
        //             GET_ACCOUNT()
        //         }
        //         REMOVE_SELECTED_ROW()
        //         classList.add('selected');
        //     }
        // });

        MODAL_ELEMENT.add(PROFILE_MODAL_ELEMENT).add(PASSWORD_MODAL_ELEMENT).on('hidden.bs.modal', (e) => {
            USER_ID = null
            FORM_ELEMENT[0].reset()
            PROFILE_FORM_ELEMENT[0].reset()
            PASSWORD_FORM_ELEMENT[0].reset()
            REMOVE_ERROR_CLASS()
            REMOVE_SELECTED_ROW()
        });

        CREATE_ELEMENT.on("click", (e) => {
            e.preventDefault();
            MODAL_ELEMENT.modal("show")
        })

        ORDER_DIRECTION_ELEMENT.on("change", () => {
            DATATABLE.ajax.reload()
        })
        ORDER_BY_ELEMENT.on("change", () => {
            DATATABLE.ajax.reload()
        })

        const REMOVE_SELECTED_ROW = () => {
            DATATABLE.rows('.selected').nodes().each((row) => row.classList.remove('selected'));
        }

        // ============== Form events
        // Normal form inputs
        let PROFILE_FIRST_NAME_INPUT = $('#profile-form input[name="first_name"]'),
            PROFILE_LAST_NAME_INPUT = $('#profile-form input[name="last_name"]'),
            PROFILE_MIDDLE_NAME_INPUT = $('#profile-form input[name="middle_name"]'),
            PROFILE_EMAIL_INPUT = $('#profile-form input[name="email"]'),
            PROFILE_ROLE_INPUT = $('#profile-form select[name="role"]'),
            PROFILE_CONTACT_NO_INPUT = $('#profile-form input[name="contact_no"]'),
            PROFILE_ADDRESS_INPUT = $('#profile-form textarea[name="address"]')

        DATATABLE.on("click", ".update-is-active", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let ID = $(e.target).data("id")
            let VALUE = $(e.target).data("value")
            SWAL_CONFIRM(`${(VALUE === 0) ? "Lock":"Unlock"}`, `Are you sure you want to ${(VALUE === 0) ? "Lock":"Unlock"} this account?`, "warning", () => {
                MAKE_REQUEST(`${BASE_URL}accounts/updateIsActive/${ID}`, "POST", JSON.stringify({
                    "is_active": parseInt(VALUE)
                }))
                .then(data => {
                    toastr.success(data.message)
                    DATATABLE.ajax.reload()
                    swal.close();
                }).catch(err => {
                    toastr.error(err.responseJSON.message)
                })
            })
        })

        FORM_ELEMENT.add(PROFILE_FORM_ELEMENT).add(PASSWORD_FORM_ELEMENT).on("submit", (e) => {
            e.stopPropagation();
            e.preventDefault();
            let l = Ladda.create($(e.target).find('.ladda-button')[0]);
            l.start();
            let FD = new FormData(e.target);
            MAKE_REQUEST(`${BASE_URL}accounts/${(USER_ID == null) ? "create":`update/${USER_ID}`}`, "POST", FD)
            .then(data => {
                l.stop();
                toastr.success(data.message)
                DATATABLE.ajax.reload()
                MODAL_ELEMENT.modal("hide")
                PROFILE_MODAL_ELEMENT.modal("hide")
                PASSWORD_MODAL_ELEMENT.modal("hide")
                REMOVE_ERROR_CLASS()
            }).catch(err => {
                l.stop();
                if(err.responseJSON.key){
                    ADD_ERROR_CLASS(err.responseJSON.key)
                }
                toastr.error(err.responseJSON.message)
            })
        })
        const GET_ACCOUNT = () => {
            MAKE_REQUEST(`${BASE_URL}accounts/update/${USER_ID}`)
            .then(data => {
                PROFILE_FIRST_NAME_INPUT.val(data.first_name)
                PROFILE_LAST_NAME_INPUT.val(data.last_name)
                PROFILE_MIDDLE_NAME_INPUT.val(data.middle_name)
                PROFILE_EMAIL_INPUT.val(data.email)
                PROFILE_ROLE_INPUT.val(data.role)
                PROFILE_CONTACT_NO_INPUT.val(data.contact_no)
                PROFILE_ADDRESS_INPUT.val(data.address)

                PROFILE_MODAL_ELEMENT.modal("show")
            }).catch(err => {
                toastr.error(err.responseJSON.message)
            })
        }

        // ============== Datatable floating action menu
        $.floatingMenu({
	        selector: 'tbody tr[data-action="show-actions-menu"]',
	        items: [
		        {
	                icon: 'bi bi-person',
	                title: 'Profile',
	                action: function(event) {
                        let ID = $("table tbody").find(".selected").find(".data_id").data("id")
                        USER_ID = ID;
	                	GET_ACCOUNT()
	                }
	            },
	            {
					icon: 'bi bi-eye',
	            	title: 'Password',
	                action: function(event) {
                        let ID = $("table tbody").find(".selected").find(".data_id").data("id")
                        USER_ID = ID;
	                	PASSWORD_MODAL_ELEMENT.modal("show")
	                }
	            },
	        ]
	    });
    })
</script>
