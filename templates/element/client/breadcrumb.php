<div class="pagetitle">
    <h1><?= $this->request->getParam('controller') ?></h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><?= $this->request->getParam('prefix') ?></a></li>
            <li class="breadcrumb-item active"><?= $this->request->getParam('controller') ?></li>
        </ol>
    </nav>
</div>
