<style>
    .nav-link span.badge {
        position: relative;
        top: 0;
        left: 40%;
        display: none;
    }
</style>
<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link <?= ($this->request->getParam('controller') == "Dashboard" && $this->request->getParam('action') == "index") ? "":"collapsed" ?>" href="<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'index', 'prefix' => 'Client']) ?>">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link <?= ($this->request->getParam('controller') == "Reservations" && $this->request->getParam('action') == "index") ? "":"collapsed" ?>" href="<?= $this->Url->build(['controller' => 'Reservations', 'action' => 'index', 'prefix' => 'Client']) ?>">
                <i class="bi bi-calendar"></i>
                <span>Reservations</span>
                <span class="badge bg-danger text-light" id="reservation-badge">4</span>
            </a>
        </li>

    </ul>

</aside><!-- End Sidebar-->

<script>
    const GET_RESERVATIONS_COUNT = () => {
        MAKE_REQUEST(`${BASE_URL}reservations/getTodayReservations`)
        .then(data => {
            if(data.count > 0){
                $("#reservation-badge").css({"display":"flex"}).html(data.count)
            }
        }).catch(err => {
            toastr.error(err.responseJSON.message)
        })
    }
    $(function(){
        GET_RESERVATIONS_COUNT()
        // ====================== PUSHER FOR REALTIME
        Pusher.logToConsole = false;
        let pusher = new Pusher('760474ced8703eaea48f', {
            cluster: 'us2'
        });

        let myChannel = pusher.subscribe("<?= $userID ?>-channel");
        myChannel.bind("<?= $userID ?>-event", function(data) {
            GET_RESERVATIONS_COUNT()
            // toastr.success(data.message)
        });
    })
</script>