<!-- Navigation-->
<nav class="navbar navbar-light bg-light static-top">
    <div class="container">
        <a class="navbar-brand" href="javascript:void(0)"><?= $this->Html->image("logo.png", ["style" => "height: 1.5rem; width: 1.5rem;"]) ?> Balay Na Santiago Reservation</a>
        <div>
            <a class="btn btn-sm btn-light px-3" href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signIn', 'prefix' => false]) ?>">Sign In</a>
            <a class="btn btn-sm btn-outline-primary px-3" href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signUp', 'prefix' => false]) ?>">Sign Up</a>
        </div>
    </div>
</nav>