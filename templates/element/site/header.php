<!-- Masthead-->
<header class="masthead">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="text-center text-white">
                    <!-- Page heading-->
                    <h1 class="mb-5">Experience Balay Na Santiago, Reserve Now!</h1>
                    <!-- Signup form-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- * * SB Forms Contact Form * *-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- This form is pre-integrated with SB Forms.-->
                    <!-- To make this form functional, sign up at-->
                    <!-- https://startbootstrap.com/solution/contact-forms-->
                    <!-- to get an API token!-->
                    <form class="form-subscribe" id="contactForm" data-sb-form-api-token="API_TOKEN">
                        <!-- Email address input-->
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center"><a class="btn btn-sm btn-primary btn-xl btn-block col-8 rounded-20px" href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signIn', 'prefix' => false]) ?>" id="submitButton" type="submit">Reserve Now!</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
