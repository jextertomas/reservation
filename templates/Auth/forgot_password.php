<main>
    <div class="container">

        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-8 d-flex flex-column align-items-center justify-content-center">

                        <div class="d-flex justify-content-center py-4">
                            <a href="<?= $this->Url->build(["controller" => "Site", "action" => "index", "prefix" => false]) ?>" class="logo d-flex align-items-center w-auto">
                                <img src="assets/img/logo.png" alt="">
                                <span class="d-block d-lg-block">Balay Na Santiago</span>
                            </a>
                        </div><!-- End Logo -->

                        <div class="card mb-3">

                            <div class="card-body">

                                <div class="pt-4 pb-2">
                                    <h5 class="card-title text-center pb-0 fs-4">Forgot You Password?</h5>
                                    <p class="text-center small">Enter your email</p>
                                </div>

                                <?= $this->Form->create(NULL, ["class" => "row g-3", "novalidate", "id" => "form"]) ?>

                                    <div class="col-12">
                                        <div class="form-floating">
                                            <input type="email" name="email" class="form-control form-contol-xl shadow-sm" id="email" placeholder="Email">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary rounded-1 w-100 ladda-button rounded-20px" type="submit" data-style="zoom-out" data-color="blue">Submit</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="small mb-0">Don't have account? <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signUp', 'prefix' => false]) ?>">Create
                                                an account</a></p>
                                        <p class="small mb-0">Already have an account? <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signIn', 'prefix' => false]) ?>">Log
                                                in</a></p>
                                    </div>
                                <?= $this->Form->end() ?>

                            </div>
                        </div>

                        <div class="credits">
                            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                        </div>

                    </div>
                </div>
            </div>

        </section>

    </div>
</main><!-- End #main -->

<script>
    $(function() {
        let FORM_ELEMENT = $("#form")
        FORM_ELEMENT.on('submit', function(e) {
            e.preventDefault();
            let l = Ladda.create($(e.target).find('.ladda-button')[0]);
            l.start();
            let FD = new FormData(e.target);

            MAKE_REQUEST(`${BASE_URL}auth/forgotPassword`, "POST", FD)
            .then(data => {
                l.stop();
                toastr.success(data.message)
            }).catch(err => {
                l.stop();
                if(err.responseJSON.key){
                    ADD_ERROR_CLASS(err.responseJSON.key)
                }
                toastr.error(err.responseJSON.message)
            })
        })
    })
</script>
