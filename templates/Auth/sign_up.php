<!-- Modal/s -->
<div class="modal fade" id="modal" tabindex="-1">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Balay Na Santiago Terms and Conditions of use.</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
        <div class="modal-body">
            <ul>
                <li>User agree on providing his/her private information to Balay Na Santiago.</li>
                <li>Balay Na Santiago should and will not share or sell private informations that the user provided to them.</li>
                <li>Other terms of use and conditions.</li>
            </ul>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary rounded-20px" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary rounded-20px" id="accept">Accept</button>
            </div>
        </div>
    </div>
</div><!-- End Full Screen Modal-->

<main>
    <div class="container">

        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12 d-flex flex-column align-items-center justify-content-center">

                        <div class="d-flex justify-content-center py-4">
                            <a href="<?= $this->Url->build(["controller" => "Site", "action" => "index", "prefix" => false]) ?>" class="logo d-flex align-items-center w-auto">
                                <img src="assets/img/logo.png" alt="">
                                <span class="d-block d-lg-block">Balay Na Santiago</span>
                            </a>
                        </div><!-- End Logo -->

                        <div class="card mb-3">

                            <div class="card-body">

                                <div class="pt-4 pb-2">
                                    <h5 class="card-title text-center pb-0 fs-4">Create an Account</h5>
                                    <p class="text-center small">Enter your personal details to create account</p>
                                </div>

                                <?= $this->Form->create(NULL, ["class" => "row g-3", "novalidate", "id" => "form"]) ?>
                                    <div class="col-4">
                                        <div class="form-floating">
                                            <input type="text" name="last_name" class="form-control form-contol-xl shadow-sm" id="last_name" placeholder="Last Name">
                                            <label for="last_name">Last Name</label>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="form-floating">
                                            <input type="text" name="first_name" class="form-control form-contol-xl shadow-sm" id="first_name" placeholder="First Name">
                                            <label for="first_name">First Name</label>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="form-floating">
                                            <input type="text" name="middle_name" class="form-control form-contol-xl shadow-sm" id="middle_name" placeholder="Middle Name">
                                            <label for="middle_name">Middle Name</label>
                                        </div>
                                    </div>

                                    <div class="col-8">
                                        <div class="form-floating">
                                            <textarea class="form-control form-contol-xl shadow-sm" name="address" placeholder="Address" id="address" cols="30" rows="1" placeholder="Address"></textarea>
                                            <label for="address">Address</label>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <div class="form-floating">
                                            <input type="text" name="contact_no" class="form-control form-contol-xl shadow-sm" id="contact_no" placeholder="Contact Number">
                                            <label for="contact_no">Contact Number</label>
                                        </div>
                                    </div>

                                    <div class="col-12">

                                        <div class="form-floating">
                                            <input type="email" name="email" class="form-control form-contol-xl shadow-sm" id="email" placeholder="Email">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input type="password" name="password" class="form-control form-contol-xl shadow-sm" id="password" placeholder="Password">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-floating">
                                            <input type="password" name="confirm_password" class="form-control form-contol-xl shadow-sm" id="confirm_password" placeholder="Confirm Password">
                                            <label for="confirm_password">Confirm Password</label>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-check">
                                            <input class="form-check-input" name="auto_login" type="checkbox" value="1" id="auto_login">
                                            <label class="form-check-label" for="auto_login">Automatically LogIn after creating an account.</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" name="terms" type="checkbox" value="1" id="terms" required>
                                            <label class="form-check-label" for="terms">I have read, agree and accept the <a href="#" data-bs-toggle="modal" data-bs-target="#modal">terms and conditions</a>.</label>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary rounded-1 w-100 ladda-button rounded-20px" type="submit" data-style="zoom-out" data-color="blue">Create Account</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="small mb-0">Already have an account? <a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'signIn', 'prefix' => false]) ?>">Log
                                                in</a></p>
                                    </div>
                                <?= $this->Form->end() ?>
                                <!-- <button class="ladda-button" data-color="green" data-style="expand-right">Submit</button> -->
                            </div>
                        </div>

                        <div class="credits">
                            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                        </div>

                    </div>
                </div>
            </div>

        </section>

    </div>
</main><!-- End #main -->

<script>
    $(function(){
        let FORM_ELEMENT = $("#form")
        FORM_ELEMENT.on("submit", (e) => {
            e.preventDefault();
            let l = Ladda.create($(e.target).find(".ladda-button")[0]);
            l.start();
            let FD = new FormData(e.target);

            MAKE_REQUEST(`${BASE_URL}auth/signUp`, "POST", FD)
            .then(data => {
                l.stop();
                toastr.success(data.message)
                setTimeout(() => {
                    window.location = `${BASE_URL}${data.url}`
                }, 500);
            }).catch(err => {
                l.stop();
                if(err.responseJSON.key){
                    ADD_ERROR_CLASS(err.responseJSON.key)
                }
                toastr.error(err.responseJSON.message)
            })
        })

        $("#accept").on("click", (e) => {
            e.preventDefault()
            e.stopPropagation()
            $("#terms").prop("checked", true)
            $("#modal").modal("hide")
        })

    })
</script>
