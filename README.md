## Clone this repository into your local machine

1. Download [git](https://git-scm.com/downloads) if it is not yet installed into your local machine.
2. Navigate into your xampp, wampp or lampp htdocs/www folder and open command prompt or any terminal you want to use.

Then run below command to clone this repository into your local machine

```bash
git clone https://gitlab.com/jextertomas/reservation.git
```



## Installing packages and dependencies

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) if it is not yet installed into your local machine.
2. Navigate into the project root folder and open command prompt or any terminal you want to use.

Then run below command to install all packages and dependencies used in this project

```bash
composer install
```


## Other necessary commands to run

1. Navigate into the project bin folder and open command prompt or any terminal you want to use.

Then run below command/s

RememberMe plugin, but if showing error regarding RememberMe package, then run this command
```bash
cake plugin load RememberMe
```

Migrate RememberMe package tables
```bash
cake migrations migrate -p RememberMe
```


# Special Note:
* Upon reading this README file and this note is still here, there is a possibility that this project is not yet paid or fully paid.
