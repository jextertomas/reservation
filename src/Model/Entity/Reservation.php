<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservation Entity
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime|null $date
 * @property string|null $official_reciept
 * @property string|null $status
 * @property int|null $is_active
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Reservation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'title' => true,
        'description' => true,
        'date' => true,
        'official_reciept' => true,
        'status' => true,
        'payment_status' => true,
        'classification' => true,
        'attachment' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
    ];
}
