<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reservations Model
 *
 * @method \App\Model\Entity\Reservation newEmptyEntity()
 * @method \App\Model\Entity\Reservation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Reservation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reservation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reservation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Reservation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reservation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reservation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reservation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reservation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Reservation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Reservation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Reservation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReservationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('reservations');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notEmptyString('user_id');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->notEmptyString('title', ucwords('event title is required.'));

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->notEmptyString('description', ucwords('event description is required.'));

        $validator
            // ->dateTime('date', [], "Invalid date format")
            ->notEmptyString('date', ucwords('event date is required.'));

        $validator
            ->allowEmptyFile('attachment-file')
            ->add('attachment-file', 'mimeType', [
                'rule' => ['mimeType', ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']],
                'message' => ucwords('Please upload a valid PDF or Word file')
            ])
            ->add('attachment-file', 'fileSize', [
                'rule' => ['fileSize', '<=', '2MB'],
                'message' => ucwords('The file size must be less than 2MB')
            ]);

        $validator
            ->scalar('official_reciept')
            ->maxLength('official_reciept', 255)
            ->allowEmptyString('official_reciept');

            
        $validator
            ->scalar('attachment')
            ->maxLength('attachment', 255)
            ->allowEmptyString('attachment');

        $validator
            ->scalar('classification')
            ->maxLength('classification', 255)
            ->allowEmptyString('classification')
            ->add('classification', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value === 'private' || $value === 'public') {
                        return true;
                    } return false;
                },
                'message' => ucwords('Classification must be either Private or public.')
            ]);

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            // ->allowEmptyString('status')
            ->add('status', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value === 'pending' || $value === 'approved' || $value === 'disapproved') {
                        return true;
                    } return false;
                },
                'message' => ucwords('Role must only be pending, approved or disapproved.')
            ]);

        $validator
            ->scalar('payment_status')
            ->maxLength('payment_status', 255)
            ->allowEmptyString('payment_status')
            ->add('payment_status', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value === 'paid' || $value === 'unpaid' || $value === 'partial' || $value === 'processing') {
                        return true;
                    } return false;
                },
                'message' => ucwords('Role must only be paid, unpaid, partial or processing.')
            ]);


        $validator
            ->allowEmptyFile('image-field')
            ->add('image-field', 'mimeType', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/jpg']],
                'message' => ucwords('Please upload a valid image (JPEG, PNG, JPG).')
            ])
            ->add('image-field', 'fileSize', [
                'rule' => ['fileSize', '<=', '2MB'],
                'message' => ucwords('The file size must be less than 2MB')
            ]);

        $validator
            ->integer('is_active')
            ->allowEmptyString('is_active')
            ->add('is_active', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value === '1' || $value === '0' || $value === 1 || $value === 0) {
                        return true;
                    } return false;
                },
                'message' => ucwords('Can\'t Lock or Unlock account.')
            ]);;

        return $validator;
    }


    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id', 'message' => ucwords("This reservation does not exists.")]);

        return $rules;
    }
}
