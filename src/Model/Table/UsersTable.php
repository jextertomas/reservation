<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Reservations', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create', ucwords('Last Name is required.'))
            ->notEmptyString('last_name', ucwords('Please fill Last Name.'));

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create', ucwords('First Name is required.'))
            ->notEmptyString('first_name', ucwords('Please fill First Name.'));

        $validator
            ->scalar('middle_name')
            ->maxLength('middle_name', 255)
            ->requirePresence('middle_name', 'create', ucwords('Middle Name is required.'))
            ->notEmptyString('middle_name', ucwords('Please fill Middle Name.'));

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->requirePresence('address', 'create', ucwords('address is required.'))
            ->notEmptyString('address', ucwords('Please fill Address.'));

        $validator
            ->scalar('contact_no')
            ->maxLength('contact_no', 255)
            ->requirePresence('contact_no', 'create', ucwords('contact number is required.'))
            ->notEmptyString('contact_no', ucwords('Please fill Contact Number.'));

        $validator
            ->email('email')
            ->requirePresence('email', 'create', ucwords('email is required.'))
            ->notEmptyString('email', ucwords('Please fill Email.'));

        $validator
            ->notEmptyString('current_password', ucwords('Please fill Current Password.'))
            ->add('current_password', 'checkCurrentPassword', [
                'rule' => function ($value, $context) {
                    $userPass = TableRegistry::getTableLocator()->get('Users')->find()->where(['id = ' => $context['data']['id']])->first()->password;
                    if(password_verify($value, $userPass)){
                        return true;
                    } return false;
                },
                'message' => ucwords('Current password is incorrect.')
            ]);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create', ucwords('password is required.'))
            ->notEmptyString('password', ucwords('Please fill Password.'))
            ->add('password', 'checkPasswordRegex', [
                'rule' => function ($value, $context) {
                    $pattern = '/^(?=.*\d.*\d).{8,}$/';
                    if (preg_match($pattern, $value)) {
                        return true;
                    } return false;
                },
                'message' => ucwords('Password must be 8 characters length and contains atleast 2 digits.')
            ])
            ->add('password', 'checkPasswordSpaces', [
                'rule' => function ($value, $context) {
                    $pattern = '/^\s$/';
                    if (strpos($value, ' ') === false) {
                        return true;
                    } return false;
                },
                'message' => ucwords('Password must not contain spaces.')
            ]);


        $validator
            ->add('confirm_password', 'confirmPassword', [
                'rule' => function ($value, $context) {
                    $password = $context['data']['password'];
                    if($value != $password){
                        return false;
                    } return true;
                },
                'message' => ucwords('Confirm Password does not match Password.')
            ]);

        $validator
            ->scalar('role')
            ->maxLength('role', 255)
            ->allowEmptyString('role')
            ->add('role', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value === 'admin' || $value === 'client') {
                        return true;
                    } return false;
                },
                'message' => ucwords('Role must only be either admin or client.')
            ]);

        $validator
            ->integer('is_active')
            ->allowEmptyString('is_active');

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->allowEmptyString('token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email', 'message' => ucwords("email is already in use.")]);

        return $rules;
    }
}
