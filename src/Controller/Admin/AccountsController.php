<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;

class AccountsController extends AppController
{
    protected $Users;

    public function initialize(): void {
        $this->viewBuilder()->setLayout('admin/default');
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
    }

    public function index(){
        $this->render("/Admin/Accounts/index");
    }
    public function profile(){
        $this->render("/Admin/Accounts/profile");
    }

    public function getAllUsers(){
        $pageSize = $this->request->getQuery()['length'];
        $start = ($this->request->getQuery()['page'] - 1) * $pageSize;
        $COLUMN_TO_SORT = $this->request->getQuery()['order_by'];
        $COLUMN_DIRECTION = $this->request->getQuery()['order_direction'];
        $SEARCH_VALUE = $this->request->getQuery()['search']['value'];

        try {
            $data = $this->Users->find('all')->where(function (QueryExpression $exp) use ($SEARCH_VALUE) {
                return $exp->or(function (QueryExpression $or) use ($SEARCH_VALUE) {
                    return $or->like("Users.first_name", "%$SEARCH_VALUE%")
                        ->like("Users.last_name", "%$SEARCH_VALUE%")
                        ->like("Users.middle_name", "%$SEARCH_VALUE%")
                        ->like("Users.role", "%$SEARCH_VALUE%")
                        ->like("Users.email", "%$SEARCH_VALUE%");
                });
            })->order(["Users.$COLUMN_TO_SORT" => $COLUMN_DIRECTION])->all();
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                'data' => array_slice($data->toArray(), $start, ($pageSize == -1) ? null:intval($pageSize)),
                'recordsTotal' => count($data),
                'recordsFiltered' => count($data),
                'draw' => $this->request->getQuery('draw'),
            ]))
            ->withStatus(200);
    }


    public function create() {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            if ($this->Users->save($user)) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Account has been created.')]))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Account could not be created. Please try again.')]))
                ->withStatus(400);
        }
    }

    public function update($id = null) {
        try {
            $user = $this->Users->get($id, [
                'contain' => [],
            ]);
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            if ($this->Users->save($user)) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Account has been updated.')]))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Account could not be updated. Please try again.')]))
                ->withStatus(400);
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($user->toArray()))
            ->withStatus(200);
    }

    public function updateIsActive($id = null){
        try {
            $user = $this->Users->get($id, [
                'contain' => [],
            ]);
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        if($this->request->is('post')){
            $data = json_decode((string)$this->request->getBody(), true);
            $user = $this->Users->patchEntity($user, $data);
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }
            $message = ($user->is_active == 0) ? "Locked":"Unlock";
            if ($this->Users->save($user)) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords("Account has been $message.")]))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords("Account could not be $message. Please try again.")]))
                ->withStatus(400);
        }

    }

}
