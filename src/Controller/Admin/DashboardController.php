<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;
use Moment\Moment;

class DashboardController extends AppController
{
    protected $Reservations;

    public function initialize(): void
    {
        $this->viewBuilder()->setLayout('admin/default');
        parent::initialize();
        $this->Reservations = TableRegistry::getTableLocator()->get('Reservations');
    }

    public function getFullCalendarEvents()
    {

        $reservations = $this->Reservations->find('all')->where(["Reservations.status LIKE " => "%approved%"])->all();

        $formattedEvents = [];
        if (count($reservations) > 0) {
            foreach ($reservations as $event) {
                $formattedEvents[] = [
                    'id' => $event->id,
                    'title' => $event->title,
                    'start' => $event->date->format('Y-m-d H:i:s'),
                    'end' => $event->date->format('Y-m-d H:i:s'),

                ];
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($formattedEvents))
            ->withStatus(200);
    }

    public function getReservationsByMonth($chart_year = null)
    {
        $pendingDates = ["January" => 0, "February" => 0, "March" => 0, "April" => 0, "May" => 0, "June" => 0, "July" => 0, "August" => 0, "September" => 0, "October" => 0, "November" => 0, "December" => 0];
        $approvedDates = ["January" => 0, "February" => 0, "March" => 0, "April" => 0, "May" => 0, "June" => 0, "July" => 0, "August" => 0, "September" => 0, "October" => 0, "November" => 0, "December" => 0];
        $disapprovedDate = ["January" => 0, "February" => 0, "March" => 0, "April" => 0, "May" => 0, "June" => 0, "July" => 0, "August" => 0, "September" => 0, "October" => 0, "November" => 0, "December" => 0];

        $dateNow = new Moment('now');

        if ($chart_year != null) {
            $generatedYear = $dateNow->subtractYears(date('Y') - $chart_year);
        } else {
            $chart_year = date('Y');
            $generatedYear = $dateNow->subtractYears(date('Y') - $chart_year);
        }

        $query = $this->Reservations->find();

        $reservations = $query->select([
            'month' => $query->func()->monthname([
                'Reservations.date' => 'identifier'
            ]),
            'monthNumber' => $query->func()->date_format(['Reservations.date' => 'identifier', '%m']),
            'count' => $query->func()->count('*')
        ])->where(['CAST(Reservations.date as DATE) BETWEEN "' . $generatedYear->startOf('year')->format('Y-m-d') . '" AND "' . $generatedYear->endOf('year')->format('Y-m-d') . '"']);


        $pending = (clone $reservations)->where(["status" => "pending"])->all()->toArray();
        $approved = (clone $reservations)->where(["status" => "approved"])->all()->toArray();
        $disapproved = (clone $reservations)->where(["status" => "disapproved"])->all()->toArray();

        foreach ($pending as $item) :
            if ($item->month != null || $item->month != '') {
                $pendingDates[$item->month] = $item->count;
            }
        endforeach;
        foreach ($approved as $item) :
            if ($item->month != null || $item->month != '') {
                $approvedDates[$item->month] = $item->count;
            }
        endforeach;
        foreach ($disapproved as $item) :
            if ($item->month != null || $item->month != '') {
                $disapprovedDate[$item->month] = $item->count;
            }
        endforeach;

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                "pending" => array_values($pendingDates),
                "approved" => array_values($approvedDates),
                "disapproved" => array_values($disapprovedDate),
            ]))
            ->withStatus(200);
    }

    public function index()
    {
        $dateNow = new Moment('now');

        $reservationThisDay = $this->Reservations->find()->where(['CAST(Reservations.date as DATE) BETWEEN "' . $dateNow->startOf('day')->format('Y-m-d') . '" AND "' . $dateNow->endOf('day')->format('Y-m-d') . '"'])->count();
        $reservationThisMonth = $this->Reservations->find()->where(['CAST(Reservations.date as DATE) BETWEEN "' . $dateNow->startOf('month')->format('Y-m-d') . '" AND "' . $dateNow->endOf('month')->format('Y-m-d') . '"'])->count();
        $reservationThisYear = $this->Reservations->find()->where(['CAST(Reservations.date as DATE) BETWEEN "' . $dateNow->startOf('year')->format('Y-m-d') . '" AND "' . $dateNow->endOf('year')->format('Y-m-d') . '"'])->count();

        $this->set(compact('reservationThisDay', 'reservationThisMonth', 'reservationThisYear'));
        $this->render("/Admin/Dashboard/index");
    }
}
