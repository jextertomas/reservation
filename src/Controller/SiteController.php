<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;

class SiteController extends AppController
{
    protected $Reservations;

    public function initialize(): void {
        $this->viewBuilder()->setLayout('site');
        parent::initialize();
        $this->Reservations = TableRegistry::getTableLocator()->get('Reservations');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['index','getFullCalendarEvents']);
    }

    public function index() {
        $this->render("/Site/index");
    }

    public function getFullCalendarEvents(){

        $reservations = $this->Reservations->find('all')->where(["Reservations.status LIKE " => "%approved%"])->all();

        $formattedEvents = [];
        if(count($reservations) > 0){
            foreach ($reservations as $event) {
                $formattedEvents[] = [
                    'id' => $event->id,
                    'title' => $event->title,
                    'start' => $event->date->format('Y-m-d H:i:s'),
                    'end' => $event->date->format('Y-m-d H:i:s'),

                ];
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($formattedEvents))
            ->withStatus(200);
    }
}
