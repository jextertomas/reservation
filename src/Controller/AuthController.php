<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Http\Exception\NotFoundException;
use Cake\Mailer\Mailer;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Router;

class AuthController extends AppController
{
    protected $Users;
    protected $Validator;

    public function initialize(): void {
        $this->viewBuilder()->setLayout('auth');
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $this->Validator = new Validator();
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['signIn', 'signUp', 'forgotPassword', 'resetPassword']);
    }

    // Login
    public function signIn() {
        $this->request->allowMethod(['get', 'post']);
        if($this->request->is('get') && $this->request->getAttribute('identity')){
            $this->Flash->success(__('Session has already started.'));
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index', 'prefix' => ucfirst($this->request->getAttribute('identity')->role)]);
        }
        if ($this->request->is('post')) {
            $this->Validator
                ->requirePresence('email', __(ucwords('Email is required.')))
                ->notEmptyString('email', __(ucwords('Please enter your email.')))
                // ->email('email', true, __(ucwords('Please enter a valid email.')))
                ->requirePresence('password', __(ucwords('Password is required.')))
                ->notEmptyString('password', __(ucwords('Please enter your password.')));

            $errors = $this->Validator->validate($this->request->getData());
            if (!empty($errors)) {
                foreach($errors as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }else{
                $user = $this->Users->find()->where(['AND' => ['email' => $this->request->getData('email')]])->first();
                if(!$user){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['message' => ucwords('Account Not Found.')]))
                        ->withStatus(400);
                }else{
                    $result = $this->Authentication->getResult();
                    if ($result && $result->isValid()) {
                        if($this->Authentication->getIdentity()->is_active === 0){
                            $this->Authentication->logout();
                            return $this->response
                                ->withType('application/json')
                                ->withStringBody(json_encode(['message' => 'Your account has been disabled, contact system super admin or developer for more info.']))
                                ->withStatus(400);
                        }

                        $this->Flash->success(__('Session has started.'));
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['message' => ucwords("Successfully logged in. You'll be redirected soon."), 'url' => $this->request->getAttribute('identity')->role.'/']))
                            ->withStatus(200);
                    }

                    if ($this->request->is('post') && !$result->isValid()) {
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['message' => ucwords('Invalid Username Or Password.')]))
                            ->withStatus(400);
                    }
                }
            }
        }

        $this->render("/Auth/sign_in");
    }

    // Register
    public function signUp() {
        $this->request->allowMethod(['get', 'post']);
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {

            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            if(!$this->request->getData('terms')){
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['key' => 'terms', 'message' => ucwords('Please read and agree to our terms and conditions of use.')]))
                    ->withStatus(400);
            }
            if ($this->Users->save($user)) {
                if($this->request->getData('auto_login')){
                    $user = $this->Users->find()->where(['AND' => ['email' => $user->email]])->first();
                    $this->Authentication->setIdentity($user);
                    $this->Flash->success(__('Session has started.'));
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['message' => ucwords("Logged In automatically. You'll be redirected soon."), 'url' => $this->request->getAttribute('identity')->role.'/dashboard']))
                        ->withStatus(200);
                }
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Your account has been registered. Redirecting to log in page'), 'url' => 'auth/sign-in']))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Account could not be registered. Unknown error has occured.')]))
                ->withStatus(400);
        }
        $this->render("/Auth/sign_up");
    }

    // Forgot Password
    public function forgotPassword(){
        if($this->request->is('post')){
            $user = $this->Users->find()->where(['email' => $this->request->getData('email')])->first();
            if($user == null){
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Email Not found.')]))
                    ->withStatus(400);
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }
            $token = uniqid().uniqid().uniqid().uniqid();
            $user->token = $token;
            if ($this->Users->save($user)) {
                $mailer = new Mailer('default');
                $mailer->setTransport('default');
                $mailer->setFrom(['retxej112600@gmail.com' => 'Forgot Password Mailer'])
                    ->setTo($user->email)
                    ->setEmailFormat('html')
                    ->setSubject('Password Reset')
                    ->viewBuilder()
                    ->setTemplate('default')
                    ->setLayout('default');
                $url = Router::fullBaseUrl().Router::url(['prefix' => false,'controller' => 'Auth', 'action' => 'resetPassword'])."/$token";
                $mailer->setViewVars(['url' => $url]);
                $mailer->deliver();

                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('An email will be sent to you after, if you did not receive an email, please re-submit your email')]))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Unknow error has occured. Please try again.')]))
                ->withStatus(400);
        }
        $this->render("/Auth/forgot_password");
    }

    // Reset Password
    public function resetPassword($token = null){
        if($token == null){
            throw new NotFoundException("Token is empty.");
        }
        $user = $this->Users->find()->where(['token' => $token])->first();
        if($user == null){
            throw new NotFoundException("Invalid Token.");
        }
        if($this->request->is('post')){
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }
            $user->token = NULL;
            if ($this->Users->save($user)) {
                if($this->request->getData('auto_login')){
                    $user = $this->Users->find()->where(['AND' => ['email' => $user->email]])->first();
                    $this->Authentication->setIdentity($user);
                    $this->Flash->success(__('Session has started.'));
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['message' => ucwords("Logged In automatically. You'll be redirected soon."), 'url' => $this->request->getAttribute('identity')->role.'/dashboard']))
                        ->withStatus(200);
                }
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Your password has been reset. Redirecting to log in page'), 'url' => 'auth/sign-in']))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }

            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Unknow error has occured. Please try again.')]))
                ->withStatus(400);
        }
        $this->set(compact('token'));
        $this->render("/Auth/reset_password");
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result && $result->isValid()) {
            $this->Authentication->logout();
            $this->Flash->success(__('Session has been ended.'));
            return $this->redirect(['controller' => 'Auth', 'action' => 'signIn', 'prefix' => false]);
        }
    }
}
