<?php
declare(strict_types=1);

namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;

class AccountsController extends AppController
{
    protected $Users;

    public function initialize(): void {
        $this->viewBuilder()->setLayout('client/default');
        parent::initialize();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
    }

    public function profile(){
        $this->render("/Client/Accounts/profile");
    }

    public function update($id = null) {
        try {
            $user = $this->Users->get($id, [
                'contain' => [],
            ]);
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user, $data);
            if($user->hasErrors()){
                foreach($user->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            if ($this->Users->save($user)) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Account has been updated.')]))
                    ->withStatus(200);
            }else{
                if($user->hasErrors()){
                    foreach($user->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Account could not be updated. Please try again.')]))
                ->withStatus(400);
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($user->toArray()))
            ->withStatus(200);
    }
}
