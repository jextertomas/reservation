<?php
declare(strict_types=1);

namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;
use Cake\Http\Exception\NotFoundException;

class ReservationsController extends AppController
{
    protected $Reservations;

    public function initialize(): void {
        $this->viewBuilder()->setLayout('client/default');
        parent::initialize();
        $this->Reservations = TableRegistry::getTableLocator()->get('Reservations');
    }

    public function index() {
        $this->render("/Client/Reservations/index");
    }

    public function getAllReservations(){
        $pageSize = $this->request->getQuery()['length'];
        $start = ($this->request->getQuery()['page'] - 1) * $pageSize;
        $COLUMN_TO_SORT = $this->request->getQuery()['order_by'];
        $COLUMN_DIRECTION = $this->request->getQuery()['order_direction'];
        $FILTER_BY =$this->request->getQuery()['filter_by'];
        $SEARCH_VALUE = $this->request->getQuery()['search']['value'];

        try {
            $data = $this->Reservations->find('all')->where(function (QueryExpression $exp) use ($SEARCH_VALUE) {
                return $exp->or(function (QueryExpression $or) use ($SEARCH_VALUE) {
                    return $or->like("Reservations.description", "%$SEARCH_VALUE%")
                        ->like("Reservations.title", "%$SEARCH_VALUE%")
                        ->like("Reservations.status", "%$SEARCH_VALUE%");
                });
            })->where(["Reservations.status LIKE " => "%$FILTER_BY%"])->matching('Users', function ($q) {
                return $q->where(["Users.id" => $this->Authentication->getIdentity()->id]);
            })->order(["Reservations.$COLUMN_TO_SORT" => $COLUMN_DIRECTION])->all();
            
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode([
                'data' => array_slice($data->toArray(), $start, ($pageSize == -1) ? null:intval($pageSize)),
                'recordsTotal' => count($data),
                'recordsFiltered' => count($data),
                'draw' => $this->request->getQuery('draw'),
            ]))
            ->withStatus(200);
    }

    public function getAllSelectedDates(){
        try {
            $data = $this->Reservations->find('all')->all();
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        $dates = $data->map(function ($item) {
            return $item->date->format('Y-m-d');
        })->toArray();

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($dates))
            ->withStatus(200);
    }
    
    public function getTodayReservations(){
        $data = $this->Reservations->find('all')->where(["DATE(Reservations.created)" => date('Y-m-d')])->where(["Reservations.status" => "approved"])->count();
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode(["count" => $data]))
            ->withStatus(200);
    }



    public function create() {
        $reservation = $this->Reservations->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['date'] = ($data['date'] != '' || $data['date'] != NULL) ? date('Y-m-d H:i:s', strtotime($data['date'])):"";
            $reservation = $this->Reservations->patchEntity($reservation, $data);
            if($reservation->hasErrors()){
                foreach($reservation->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            // There might error occur while handling file, just to be sure check the handling of files
            try {
                $image = $data['image-field'];
                if ($image->getSize() > 0) {
                    $array = explode("/",$image->getClientMediaType());
                    $name = uniqid().uniqid().uniqid().'.'.$array[1];
                    $image->moveTo(WWW_ROOT.'reciept-images'.DS.$name);
                    $reservation->official_reciept = $name;
                    $reservation->payment_status = "processing";
                }else{
                    $reservation->official_reciept = NULL;
                    $reservation->payment_status = "unpaid";
                }
                $image = $data['attachment-file'];
                if ($image->getSize() > 0) {
                    $array = explode("/",$image->getClientMediaType());
                    $name = uniqid().uniqid().uniqid().'.'.$array[1];
                    $image->moveTo(WWW_ROOT.'attachment-images'.DS.$name);
                    $reservation->attachment = $name;
                }else{
                    $reservation->attachment = NULL;
                }
            } catch (\Exception $e) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                    ->withStatus(400);
            }

            $reservation->user_id = $this->Authentication->getIdentity()->id;
            $reservation->status = "pending";

            if ($this->Reservations->save($reservation)) {
                $options = array(
                    'cluster' => 'us2',
                    'useTLS' => true
                );
                $pusher = new \Pusher\Pusher(
                    '760474ced8703eaea48f',
                    '0d494032f1c69c2ddac1',
                    '1803084',
                    $options
                );
                
                $pusher->trigger('client-channel', 'client-event', [
                    "message" => ucwords("A client has created a reservation.")
                ]);

                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Reservation has been created.')]))
                    ->withStatus(200);
            }else{
                if($reservation->hasErrors()){
                    foreach($reservation->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Reservation could not be created. Please try again.')]))
                ->withStatus(400);
        }
    }

    public function reSubmit($id = null){
        try {
            $reservation = $this->Reservations->get($id, [
                'contain' => [],
            ]);
            $reservation->status = "pending";
            if ($this->Reservations->save($reservation)) {
                $options = array(
                    'cluster' => 'us2',
                    'useTLS' => true
                );
                $pusher = new \Pusher\Pusher(
                    '760474ced8703eaea48f',
                    '0d494032f1c69c2ddac1',
                    '1803084',
                    $options
                );
                
                $pusher->trigger('client-channel', 'client-event', [
                    "message" => ucwords("A client has re-submtted a reservation.")
                ]);
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Reservation has been re-submitted.')]))
                    ->withStatus(200);
            }else{
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('An error has occured while re-submitting reservation, please try again.')]))
                    ->withStatus(400);
            }
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }
    }

    public function update($id = null) {
        try {
            $reservation = $this->Reservations->get($id, [
                'contain' => [],
            ]);
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));
            $reservation = $this->Reservations->patchEntity($reservation, $data);
            if($reservation->hasErrors()){
                foreach($reservation->getErrors() as $key => $value){
                    return $this->response
                        ->withType('application/json')
                        ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                        ->withStatus(400);
                    break;
                }
            }

            // There might error occur while handling file, just to be sure check the handling of files
            try {
                $image = $data['image-field'];
                if ($image->getSize() > 0) {
                    if($reservation->official_reciept){
                        if (file_exists(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept)) {
                            unlink(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept);
                        }
                    }

                    $array = explode("/",$image->getClientMediaType());
                    $name = uniqid().uniqid().uniqid().'.'.$array[1];
                    $image->moveTo(WWW_ROOT.'reciept-images'.DS.$name);
                    $reservation->official_reciept = $name;
                    $reservation->payment_status = "processing";
                }
                if(filter_var($data["will-delete-image"], FILTER_VALIDATE_BOOLEAN)){
                    if (file_exists(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept)) {
                        unlink(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept);
                    }
                    $reservation->official_reciept = NULL;
                    $reservation->payment_status = "unpaid";
                }

                $image = $data['attachment-file'];
                if ($image->getSize() > 0) {
                    if($reservation->attachment){
                        if (file_exists(WWW_ROOT.'attachment-images'.DS.$reservation->attachment)) {
                            unlink(WWW_ROOT.'attachment-images'.DS.$reservation->attachment);
                        }
                    }

                    $array = explode("/",$image->getClientMediaType());
                    $name = uniqid().uniqid().uniqid().'.'.(($array[1] != "pdf") ? "docx":"pdf");
                    $image->moveTo(WWW_ROOT.'attachment-images'.DS.$name);
                    $reservation->attachment = $name;
                }

            } catch (\Exception $e) {
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                    ->withStatus(400);
            }



            if ($this->Reservations->save($reservation)) {
                $options = array(
                    'cluster' => 'us2',
                    'useTLS' => true
                );
                $pusher = new \Pusher\Pusher(
                    '760474ced8703eaea48f',
                    '0d494032f1c69c2ddac1',
                    '1803084',
                    $options
                );
                
                $pusher->trigger('client-channel', 'client-event', [
                    "message" => ucwords("A client has updated a reservation.")
                ]);
                return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Reservation has been updated.')]))
                    ->withStatus(200);
            }else{
                if($reservation->hasErrors()){
                    foreach($reservation->getErrors() as $key => $value){
                        return $this->response
                            ->withType('application/json')
                            ->withStringBody(json_encode(['key' => $key, 'message' => ucwords(reset($value))]))
                            ->withStatus(400);
                        break;
                    }
                }
            }
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Reservation could not be updated. Please try again.')]))
                ->withStatus(400);
        }
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($reservation->toArray()))
            ->withStatus(200);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        try {
            $reservation = $this->Reservations->get($id);
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }
        try {
            if($reservation->official_reciept){
                if (file_exists(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept)) {
                    unlink(WWW_ROOT.'reciept-images'.DS.$reservation->official_reciept);
                }
            }
            
            if($reservation->attachment){
                if (file_exists(WWW_ROOT.'attachment-images'.DS.$reservation->attachment)) {
                    unlink(WWW_ROOT.'attachment-images'.DS.$reservation->attachment);
                }
            }
        } catch (\Exception $e) {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords($e->getMessage())]))
                ->withStatus(400);
        }


        if ($this->Reservations->delete($reservation)) {
            $options = array(
                'cluster' => 'us2',
                'useTLS' => true
            );
            $pusher = new \Pusher\Pusher(
                '760474ced8703eaea48f',
                '0d494032f1c69c2ddac1',
                '1803084',
                $options
            );
            
            $pusher->trigger('client-channel', 'client-event', [
                "message" => ucwords("A client has deleted a reservation.")
            ]);
            return $this->response
                    ->withType('application/json')
                    ->withStringBody(json_encode(['message' => ucwords('Reservation has been deleted.')]))
                    ->withStatus(200);
        } else {
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode(['message' => ucwords('Reservation could not be deleted. Please try again.')]))
                ->withStatus(400);
        }
    }
    
    public function download($filename)
    {
        $filePath = WWW_ROOT.'attachment-images'.DS.$filename;

        if (!file_exists($filePath)) {
            throw new NotFoundException(ucwords("File not found or there is no file uploaded yet."));
        }

        $this->response = $this->response->withFile($filePath, ['download' => true, 'name' => $filename]);
        
        return $this->response;
    }
}
