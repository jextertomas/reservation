$(function() {
	$("input, textarea, select").on("focus", e => {
		$(e.target).removeClass("error-field");
	});
});

const CHECK_FILE_SIZE = (e) => {
    var fileInput = e;
    console.log(e.files.length)
    if (fileInput.files.length > 0) {
        var fileSize = fileInput.files[0].size;
        var maxSize = 2 * 1024 * 1024;

        if (fileSize > maxSize) {
            toastr.error('The selected file exceeds the maximum allowed size of 2 megabytes.')
            fileInput.value = '';
            return false
        }
    }
}

const REMOVE_ERROR_CLASS = () => {
	$(".error-field").each((index, element) => {
		$(element).removeClass("error-field");
    });
    $(".error-field-file").each((index, element) => {
		$(element).removeClass("error-field-file");
    });
};

const ADD_ERROR_CLASS = FIELD => {
	$(".error-field").each((index, element) => {
		$(element).removeClass("error-field");
	});
    $(`input[name="${FIELD}"]`).addClass("error-field");
    if (FIELD == "image-field") {
	    $(`label[for="${FIELD}"]`).addClass("error-field-file"); 
    }
	$(`textarea[name="${FIELD}"]`).addClass("error-field");
	$(`select[name="${FIELD}"]`).addClass("error-field");
};

const RE_INIT_TOOLTIP = () => {
	let tooltipTriggerList = [].slice.call(
		document.querySelectorAll('[data-bs-toggle="tooltip"]')
	);
	let tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl);
	});
};

const SWAL_CONFIRM = (TITLE, TEXT, ICON, CALLBACK) => {
	swal({
		title: TITLE,
		text: TEXT,
		icon: ICON,
		buttons: {
			cancel: "Cancel",
			confirm: {
				text: "Confirm",
				value: true,
				visible: true,
				className: "confirm-btn",
				closeModal: false
			}
		},
		closeOnClickOutside: false,
		closeOnEsc: false,
		dangerMode: true
	}).then(confirm => {
		if (confirm) {
			CALLBACK();
		}
	});
};

const SWAL_OPTIONS = (TITLE, TEXT, ICON, RE_SUBMIT_CALLBACK, DELETE_CALLBACK) => {
    swal({
		title: TITLE,
		text: TEXT,
		icon: ICON,
		buttons: {
			cancel: "Cancel!",
			reSubmit: {
				text: "Re Submit",
				value: "reSubmit",
				visible: true,
				className: "bg-primary",
				closeModal: false
			},
			confirm: {
				text: "Delete",
				value: "delete",
				visible: true,
				className: "bg-danger",
				closeModal: false
			}
		},
		closeOnClickOutside: false,
		closeOnEsc: false,
		dangerMode: true
	}).then(value => {
		switch (value) {
			case "delete":
				DELETE_CALLBACK()
				break;

			case "reSubmit":
				RE_SUBMIT_CALLBACK()
				break;

			default:
                swal.close();
		}
	});
};

const MAKE_REQUEST = (URL, TYPE = "GET", DATA) => {
	return new Promise((resolve, reject) => {
		$.ajax({
			processData: false,
			contentType: false,
			url: `${URL}`,
			type: TYPE,
			dataType: "JSON",
			headers: {
				"X-CSRF-Token": $('[name="csrfToken"]').attr("content"),
				Accept: "application/json"
			},
			data: DATA
		})
			.done(function(data, status, xhr) {
				resolve(data);
			})
			.fail(function(data, status, xhr) {
				reject(data);
			});
	});
};
