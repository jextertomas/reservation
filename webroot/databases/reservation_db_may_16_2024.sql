/*
 Navicat Premium Data Transfer

 Source Server         : wamppserver
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : reservation_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 16/05/2024 12:50:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013, 'CreateRememberMeTokens', '2024-04-11 01:56:32', '2024-04-11 01:56:32', 0);

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (1, '2024-04-11 10:30:22', '2024-04-11 10:30:22', 'Users', '0', '254a99610de096935ca9736f0c180caf7f342d8a', '4f512240e1e61918ce8d19ffc0a7e918402ae501', '2024-05-11 10:30:22');
INSERT INTO `remember_me_tokens` VALUES (2, '2024-04-11 13:43:19', '2024-04-11 13:43:19', 'Users', '4', 'd3f2c4c5e4b120fea4b47489104a3245896c0a28', '87881ec37f35d04de5476f1ffbedf0a3c2eb75bb', '2024-05-11 13:43:19');
INSERT INTO `remember_me_tokens` VALUES (3, '2024-04-21 20:57:57', '2024-04-21 20:57:57', 'Users', '8', 'e9434f9907c8c103e11f67287bec568a54a3ca35', '46d805114fd8676248cb6f38bf4870adeb930e2f', '2024-05-21 20:57:57');

-- ----------------------------
-- Table structure for reservations
-- ----------------------------
DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `date` datetime NULL DEFAULT NULL,
  `official_reciept` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL COMMENT 'pending, approved, disapproved, cancelled',
  `payment_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL COMMENT 'paid, unpaid, partial, processing',
  `classification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `attachment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `is_active` int NULL DEFAULT 1,
  `created` datetime NULL DEFAULT NULL,
  `modified` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of reservations
-- ----------------------------
INSERT INTO `reservations` VALUES (2, 8, 'Exercitation eos fug', 'Esse atque qui dele', '2024-05-16 00:00:00', NULL, 'approved', 'paid', 'private', '66446a3100be466446a3100be866446a3100bea.pdf', 1, '2024-05-15 07:25:34', '2024-05-15 08:01:22');
INSERT INTO `reservations` VALUES (14, 9, 'Dolore similique ea ', 'Qui cumque ipsum asp', '2024-05-01 00:00:00', NULL, 'approved', 'paid', 'public', NULL, 1, '2024-05-16 02:44:31', '2024-05-16 03:03:34');
INSERT INTO `reservations` VALUES (19, 8, 'asd', 'asd', '2024-05-08 00:00:00', NULL, 'pending', 'processing', 'public', NULL, 1, '2024-05-16 03:02:25', '2024-05-16 03:02:25');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `middle_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `contact_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT 'client' COMMENT 'Admin or Client',
  `is_active` int NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `created` datetime NULL DEFAULT NULL,
  `modified` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_520_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (8, 'Geraldine', 'Sutton', 'Barry Webster', 'admin@gmail.com', '$2y$10$lcSkQHYRi36fvtedae4rnuFyjsEoP5Y23qSZAMb3CBvTQn5WKAYQa', 'Minim eligendi debit', '936', 'admin', 1, NULL, '2024-04-11 06:29:13', '2024-04-21 05:25:24');
INSERT INTO `users` VALUES (9, 'Ishmael', 'Mayer', 'Damon Alvarez', 'client@gmail.com', '$2y$10$yRorIRxmy6U5ZlRNVJ4VfehZ2Oz2KiNawhO6qwiPvh7kCalVblO/q', 'Sunt ad sit enim d', '973', 'client', 1, NULL, '2024-04-11 06:30:30', '2024-04-23 03:09:13');

SET FOREIGN_KEY_CHECKS = 1;
